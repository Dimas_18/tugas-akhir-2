<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;
use CodeIgniter\Filters\CSRF;
use CodeIgniter\Filters\DebugToolbar;
use CodeIgniter\Filters\Honeypot;
use CodeIgniter\Filters\InvalidChars;
use CodeIgniter\Filters\SecureHeaders;
use App\Filters\Admin;
use App\Filters\Users;

class Filters extends BaseConfig
{
    /**
     * Configures aliases for Filter classes to
     * make reading things nicer and simpler.
     */
    public array $aliases = [
        'csrf'          => CSRF::class,
        'toolbar'       => DebugToolbar::class,
        'honeypot'      => Honeypot::class,
        'invalidchars'  => InvalidChars::class,
        'secureheaders' => SecureHeaders::class,
        'admin' => Admin::class,
        'users' => Users::class,
    ];

    /**
     * List of filter aliases that are always
     * applied before and after every request.
     */
    public array $globals = [
        'before' => [
            // 'honeypot',
            // 'csrf',
            // 'invalidchars',
            'admin' => [
                'except' => [
                    '/', 'Web', 'Web/*',
                    'Auth', 'Auth/*',
                ]
            ],
            'users' => [
                'except' => [
                    '/', 'Web', 'Web/*',
                    'Auth', 'Auth/*',
                ]
            ],
        ],
        'after' => [
            'toolbar',
            // 'honeypot',
            // 'secureheaders',
            'admin' => [
                'except' => [
                    '/',
                    'Home', 'Home/*', 'Home/index', 'Home/tampil_data', 'Home/tambah_data', 'Home/masukan_data', 'Home/hapus_data', 'Home/tentang',
                    'Import', 'Import/*',
                    'Export', 'Export/*', 'Export/cetak_pdf', 'Export/cetak_xlsx',
                    'User', 'User/*', 'User/tampil_pengguna', 'User/tambah_pengguna', 'User/masukan_pengguna', 'User/hapus_pengguna',
                    'Map', 'Map/*', 'Map/index', 'Map/cari',
                    'Other', 'Other/*', 'Other/tentang'
                ]
            ],
            'users' => [
                'except' => [
                    '/',
                    'Home', 'Home/*', 'Home/index', 'Home/tampil_data', 'Home/tambah_data', 'Home/masukan_data', 'Home/cetak_data', 'Home/hapus_data', 'Home/tentang',
                    'Import', 'Import/*',
                    'Export', 'Export/*', 'Export/cetak_pdf', 'Export/cetak_xlsx',
                    'Map', 'Map/*', 'Map/index', 'Map/cari',
                    'Other', 'Other/tentang'
                ]
            ],
        ],
    ];

    /**
     * List of filter aliases that works on a
     * particular HTTP method (GET, POST, etc.).
     *
     * Example:
     * 'post' => ['foo', 'bar']
     *
     * If you use this, you should disable auto-routing because auto-routing
     * permits any HTTP method to access a controller. Accessing the controller
     * with a method you don’t expect could bypass the filter.
     */
    public array $methods = [];

    /**
     * List of filter aliases that should run on any
     * before or after URI patterns.
     *
     * Example:
     * 'isLoggedIn' => ['before' => ['account/*', 'profiles/*']]
     */
    public array $filters = [];
}
