<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
// $routes->setDefaultController('Home');
$routes->setDefaultController('Web');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

$routes->get('/', 'Web::index');
$routes->get('/Auth/login', 'Auth::login');
$routes->post('/Auth/cek_login', 'Auth::cek_login');
$routes->get('/Auth/lupa_password', 'Auth::lupa_password', ['as' => 'lupa_password']);
$routes->post('/Auth/kirim_link_reset', 'Auth::kirim_link_reset', ['as' => 'kirim_link_reset']);
$routes->get('/Auth/proses_link/(:any)', 'Auth::proses_link/$1', ['as' => 'proses_link']);
$routes->post('/Auth/reset_password/(:any)', 'Auth::reset_password/$1', ['as' => 'reset_password']);
$routes->get('/Auth/logout', 'Auth::logout');
$routes->get('/Home', 'Home::index');
$routes->get('/Home/tampil_data', 'Home::tampil_data');
$routes->get('/Home/tambah_data', 'Home::tambah_data');
$routes->post('/Home/masukan_data', 'Home::masukan_data');
$routes->get('/Home/ubah_data/(:segment)', 'Home::ubah_data/$1');
$routes->post('/Home/hapus_banyak_data', 'Home::hapus_banyak_data');
$routes->get('/Import', 'Import::index');
$routes->post('/Import/import_data', 'Import::import_data');
$routes->get('/Export/cetak_pdf', 'Export::cetak_pdf');
$routes->get('/Export/cetak_xlsx', 'Export::cetak_xlsx');
$routes->get('/Map', 'Map::index');
$routes->get('/Map/batas_rt', 'Map::batas_rt');
$routes->get('/Map/jalan', 'Map::jalan');
$routes->get('/Map/penduduk', 'Map::penduduk');
$routes->get('/Map/cari', 'Map::cari');
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
