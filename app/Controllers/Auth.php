<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\Autentikasi;
use PhpParser\Node\Stmt\If_;
use App\Models\Token;
use Carbon\Carbon;
use Predis\Collection\Iterator\HashKey;

class Auth extends BaseController
{
    protected $helpers = ['url', 'form', 'CIMail'];
    protected $Autentikasi;
    protected $Token;

    public function __construct()
    {
        $this->Autentikasi = new Autentikasi();
        $this->Token = new Token();
    }

    public function login()
    {
        $data = [
            'judul' => 'Login'
        ];
        return view('KelolaPengguna/Login', $data);
    }

    public function cek_login()
    {
        if ($this->validate([
            'username_email' => [
                'label' => 'Username atau Email',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
            'password' => [
                'label' => 'Password',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
        ])) {
            $username_email = $this->request->getPost('username_email');
            $password = sha1($this->request->getPost('password'));
            $cek_username_email = $this->Autentikasi->login_username_email($username_email);
            if ($cek_username_email) {
                $cek_password = $this->Autentikasi->login_password($password);
                if ($cek_password) {
                    session()->set('id', $cek_username_email['id']);
                    session()->set('nama_asli', $cek_username_email['nama_asli']);
                    session()->set('username', $cek_username_email['username']);
                    session()->set('email', $cek_username_email['email']);
                    session()->set('level', $cek_username_email['level']);
                    return redirect()->to('Home');
                } else {
                    session()->setFlashdata('login_failed1', 'Login belum berhasil!. Periksa kembali Password Anda');
                    return redirect()->to('Auth/login')->withInput();
                }
            } else {
                session()->setFlashdata('login_failed2', 'Login belum berhasil!. Periksa kembali Username atau Email Anda');
                return redirect()->to('Auth/login')->withInput();
            }
        } else {
            return redirect()->to('Auth/login')->withInput();
        }
    }

    public function lupa_password()
    {
        $data = array(
            'judul' => 'Lupa Password',
            'validation' => null
        );

        return view('KelolaPengguna/LupaPassword', $data);
    }

    public function kirim_link_reset()
    {
        $telah_valid = $this->validate([
            'email' => [
                'label' => 'Email',
                'rules' => 'required|valid_email|is_not_unique[users.email]',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!',
                    'valid_email' => 'Data {field} tidak valid!',
                    'is_not_unique' => 'Data {field} tidak terdaftar!'
                ]
            ]
        ]);

        if (!$telah_valid) {
            // return view('KelolaPengguna/LupaPassword', [
            //     'judul' => 'Lupa Password',
            //     'validation' => $this->validator
            // ]);
            return redirect()->to('Auth/lupa_password')->withInput();
        } else {
            $pengguna = new Autentikasi();
            $info = $pengguna->asObject()->where('email', $this->request->getVar('email'))->first();
            $token = bin2hex(openssl_random_pseudo_bytes(65));
            $token_reset = new Token();
            $token_lama = $token_reset->asObject()->where('email', $info->email)->first();

            if ($token_lama) {
                $token_reset->where('email', $info->email)->set(['token' => $token, 'waktu_buat' => Carbon::now()])->update();
            } else {
                $token_reset->insert([
                    'email' => $info->email,
                    'token' => $token,
                    'waktu_buat' => Carbon::now()
                ]);
            }

            // $link = route_to('proses_link', $token);
            $link = base_url(route_to('proses_link', $token));
            $surat_data = array(
                'link' => $link,
                'info' => $info
            );

            $tampilan = \Config\Services::renderer();
            $surat_isi = $tampilan->setVar('surat_data', $surat_data)->render('KelolaPengguna/IsiEmailReset');

            $mailConfig = array(
                'mail_from_email' => env('EMAIL_FROM_ADDRESS'),
                'mail_from_name' => env('EMAIL_FROM_NAME'),
                'mail_recipient_email' => $info->email,
                'mail_recipient_name' => $info->nama_asli,
                'mail_subject' => 'Reset Password',
                'mail_body' => $surat_isi
            );

            if (sendEmail($mailConfig)) {
                session()->setFlashdata('send_link_reset_success', 'Kami telah mengirimkan link reset password ke email yang dituju. Harap periksa isi email yang dituju.');
                return redirect()->route('lupa_password');
            } else {
                session()->setFlashdata('send_link_reset_failed', 'Terjadi kesalahan. Silahkan coba lagi.');
                return redirect()->route('lupa_password');
            }
        }
    }

    public function proses_link($token)
    {
        $token_reset = new Token();
        $cek_token = $token_reset->asObject()->where('token', $token)->first();
        if (!$cek_token) {
            return redirect()->route('lupa_password')->with('fail', 'Token tidak terdaftar. Silakan untuk minta link reset password yang lain.');
        } else {
            $waktu_menit = Carbon::createFromFormat('Y-m-d H:i:s', $cek_token->waktu_buat)->diffInMinutes(Carbon::now());
            if ($waktu_menit > 15) {
                return redirect()->route('lupa_password')->with('fail', 'Token telah kadaluarsa. Silakan untuk minta link reset password yang lain.');
            } else {
                return view('KelolaPengguna/ResetPassword', [
                    'judul' => 'Lupa Password',
                    'validation' => null,
                    'token' => $token
                ]);
            }
        }
    }

    public function reset_password($token)
    {
        $telah_valid = $this->validate([
            'password' => [
                'label' => 'Password Baru',
                'rules' => 'required|min_length[3]|password[password]',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!',
                    'min_length' => 'Data {field} harus memuat minimal 3 karakter!',
                    'password' => 'Data {field} harus memuat minimal 1 angka, 1 huruf kecil atau besar dan 1 simbol unik!'
                ]
            ],
            'konfirmasi_password' => [
                'label' => 'Konfirmasi Password Baru',
                'rules' => 'required|matches[password]',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!',
                    'matches' => 'Data {field} harus sama sesuai yang diinputkan!'
                ]
            ]
        ]);

        if (!$telah_valid) {
            return view('KelolaPengguna/ResetPassword', [
                'judul' => 'Reset Password',
                'validation' => null,
                'token' => $token
            ]);
        } else {
            $token_reset = new Token();
            $ambil_token = $token_reset->asObject()->where('token', $token)->first();
            $pengguna = new Autentikasi();
            $info = $pengguna->asObject()->where('email', $ambil_token->email)->first();
            if (!$ambil_token) {
                return redirect()->back()->with('fail', 'Token tidak tersedia!')->withInput();
            } else {
                $pengguna->where('email', $info->email)->set(['password' => sha1($this->request->getVar('password'))])->update();
                $surat_data = array(
                    'info' => $info,
                    'password' => $this->request->getVar('password')
                );
                $tampilan = \Config\Services::renderer();
                $surat_isi = $tampilan->setVar('surat_data', $surat_data)->render('KelolaPengguna/EmailKonfirmasi');
                $mailConfig = array(
                    'mail_from_email' => env('EMAIL_FROM_ADDRESS'),
                    'mail_from_name' => env('EMAIL_FROM_NAME'),
                    'mail_recipient_email' => $info->email,
                    'mail_recipient_name' => $info->nama_asli,
                    'mail_subject' => 'Perubahan Password',
                    'mail_body' => $surat_isi
                );
                if (sendEmail($mailConfig)) {
                    $token_reset->where('email', $info->email)->delete();
                    return redirect()->to('Auth/login')->with('success', 'Password telah berhasil diubah. Silahkan login dengan password baru Anda');
                } else {
                    return redirect()->back()->with('fail', 'Terjadi Kesalahan!')->withInput();
                }
            }
        }
    }

    public function logout()
    {
        // session()->destroy();
        session()->remove('id');
        session()->remove('nama_asli');
        session()->remove('email');
        session()->remove('username');
        session()->remove('level');
        session()->setFlashdata('logout', 'Anda telah Log Out!');
        return redirect()->to('Auth/login');
    }
}
