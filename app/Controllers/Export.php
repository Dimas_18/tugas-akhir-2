<?php

namespace App\Controllers;

use App\Models\Penduduk;

use App\Controllers\BaseController;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Export extends BaseController
{
    protected $Penduduk;
    protected $helpers = ['custom'];

    public function __construct()
    {
        $this->Penduduk = new Penduduk();
    }
    
    public function cetak_pdf()
    {
        $data = [
            'judul' => 'Penduduk',
            'menu' => 'kelola_data',
            'penduduk' => $this->Penduduk->tampil_data(),
        ];
        return view('KelolaData/CetakPDF', $data);
    }

    public function cetak_xlsx()
    {
        $penduduk = $this->Penduduk->findAll();
        
        $spreadsheet = new Spreadsheet();
        $lembar = $spreadsheet->getActiveSheet();
        $lembar->setCellValue('A1', 'No.');
        $lembar->setCellValue('B1', 'NIK');
        $lembar->setCellValue('C1', 'Nama');
        $lembar->setCellValue('D1', 'Alamat');
        $lembar->setCellValue('E1', 'Jenis Kelamin');
        $lembar->setCellValue('F1', 'RW (ID)');
        $lembar->setCellValue('G1', 'RT (ID)');
        $lembar->setCellValue('H1', 'Latitude (Koordinat)');
        $lembar->setCellValue('I1', 'Longitude (Koordinat)');

        $kolom = 2;

        foreach ($penduduk as $key => $p) {
            $lembar->setCellValue('A'.$kolom, ($kolom - 1));
            $lembar->setCellValue('B'.$kolom, $p['nik']);
            $lembar->setCellValue('C'.$kolom, $p['nama']);
            $lembar->setCellValue('D'.$kolom, $p['alamat']);
            $lembar->setCellValue('E'.$kolom, $p['jenis_kelamin']);
            $lembar->setCellValue('F'.$kolom, $p['id_rw']);
            $lembar->setCellValue('G'.$kolom, $p['id_rt']);
            $lembar->setCellValue('H'.$kolom, $p['latitude']);
            $lembar->setCellValue('I'.$kolom, $p['longitude']);
            $kolom++;
        }

        $lembar->getStyle('A1:I1')->getFont()->setBold(true);
        $lembar->getStyle('A1:I1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FFFF');
        $desain = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FF00000'],
                ]
            ]
        ];

        $lembar->getStyle('A1:I'.($kolom - 1))->applyFromArray($desain);

        $lembar->getColumnDimension('A')->setAutoSize(true);
        $lembar->getColumnDimension('B')->setAutoSize(true);
        $lembar->getColumnDimension('C')->setAutoSize(true);
        $lembar->getColumnDimension('D')->setAutoSize(true);
        $lembar->getColumnDimension('E')->setAutoSize(true);
        $lembar->getColumnDimension('F')->setAutoSize(true);
        $lembar->getColumnDimension('G')->setAutoSize(true);
        $lembar->getColumnDimension('H')->setAutoSize(true);
        $lembar->getColumnDimension('I')->setAutoSize(true);

        $tulis = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=datapenduduk.xlsx');
        header('Cache-Control: max-age=0');
        $tulis->save('php://output');
        exit();
    }
}
