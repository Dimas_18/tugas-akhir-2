<?php

namespace App\Controllers;

use App\Models\Penduduk;
use App\Models\Batas;
use App\Models\Wilayah;

class Home extends BaseController
{
    protected $Penduduk;
    protected $Batas;
    protected $vb;
    protected $Wilayah;

    public function __construct()
    {
        $this->Penduduk = new Penduduk();
        $this->Batas = new Batas();
        $this->Wilayah = new Wilayah();
    }

    public function index()
    {
        $data = [
            'judul' => 'Statistik',
            'vb' => $this->vb,
            'batas' => $this->Batas->findAll(),
            'getsegment1' => $this->request->uri->getSegment(1),
            'menu' => 'beranda',
            'penduduk' => $this->Penduduk->tampil_data(),
            'rw' => $this->Wilayah->data_rw(),
            'rt' => $this->Wilayah->data_rt_2()
        ];
        return view('Beranda/index', $data);
    }

    public function tampil_data()
    {
        $data = [
            'judul' => 'Data Penduduk',
            'penduduk' => $this->Penduduk->tampil_data(),
            'menu' => 'kelola_data'
        ];
        return view('KelolaData/index', $data);
    }

    public function tambah_data()
    {

        $data = [
            'judul' => 'Input Data Penduduk',
            'menu' => 'kelola_data',
            'rw' => $this->Wilayah->data_rw()
        ];

        return view('KelolaData/TambahData', $data);
    }

    public function masukan_data()
    {
        if ($this->validate([
            'nik' => [
                'label' => 'NIK/No.KTP',
                // 'rules' => 'required|min_length[16]|is_unique[penduduk.nik]',
                'rules' => 'is_unique[penduduk.nik]',
                'errors' => [
                    // 'required' => 'Data {field} tidak boleh kosong!',
                    // 'min_length' => 'Panjang data {field} minimal 16 digit!',
                    'is_unique' => 'Data {field} sudah ada, coba input dengan data berbeda!'

                ]
            ],
            'nama' => [
                'label' => 'Nama Lengkap',
                'rules' => 'required|is_unique[penduduk.nama]',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!',
                    'is_unique' => 'Data {field} sudah ada, coba input dengan data berbeda!'
                ]
            ],
            // 'alamat' => [
            //     'label' => 'Alamat Lengkap',
            //     'rules' => 'required',
            //     'errors' => [
            //         'required' => 'Data {field} tidak boleh kosong!'
            //     ]
            // ],
            'latitude' => [
                'label' => 'Latitude',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
            'longitude' => [
                'label' => 'Longitude',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
            'foto' => [
                'label' => 'Foto',
                'rules' => 'uploaded[foto]|is_image[foto]|max_size[foto,5000]|mime_in[foto,image/jpg,image/jpeg,image/png]',
                // 'rules' => 'max_size[foto,5000]|mime_in[foto,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'uploaded' => 'Berkas {field} tidak boleh kosong!',
                    'is_image' => 'Berkas {field} harus berupa file gambar atau foto!',
                    'max_size' => 'Ukuran maksimal berkas {field} tidak lebih dari 5MB!',
                    'mime_in' => 'Format berkas {field} hanya mendukung JPG, JPEG and PNG!'
                ]
            ],
            'id_rw' => [
                'label' => 'RW',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
            'id_rt' => [
                'label' => 'RT',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
            'jenis_kelamin' => [
                'label' => 'Jenis Kelamin',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ]
        ])) {
            $foto = $this->request->getFile('foto');
            $nama_foto = $foto->getRandomName();
            $data = [
                'nik' => $this->request->getPost('nik'),
                'nama' => $this->request->getPost('nama'),
                'alamat' => $this->request->getPost('alamat'),
                'latitude' => $this->request->getPost('latitude'),
                'longitude' => $this->request->getPost('longitude'),
                'foto' => $nama_foto,
                'id_rw' => $this->request->getPost('id_rw'),
                'id_rt' => $this->request->getPost('id_rt'),
                'jenis_kelamin' => $this->request->getPost('jenis_kelamin')
            ];
            $foto->move('foto', $nama_foto);
            $this->Penduduk->masukan_data($data);
            session()->setFlashdata('add_data_success', 'Data berhasil ditambahkan!');
            return redirect()->to('Home/tampil_data');
        } else {
            return redirect()->to('Home/tambah_data')->withInput();
        }
    }

    public function ubah_data($id)
    {

        $data = [
            'judul' => 'Edit Data',
            'penduduk' => $this->Penduduk->data_id($id),
            'menu' => 'kelola_data',
            'rw' => $this->Wilayah->data_rw()
        ];

        return view('KelolaData/UbahData', $data);
    }

    public function perbarui_data($id = null)
    {
        $nik_lama = $this->Penduduk->ambil_data($this->request->getVar('id'));
        if ($nik_lama['nik'] == $this->request->getVar('nik')) {
            $aturan_nik = 'required';
        } else {
            $aturan_nik = 'required|is_unique[penduduk.nik]';
        }
        $nama_lama = $this->Penduduk->ambil_data($this->request->getVar('id'));
        if ($nama_lama['nama'] == $this->request->getVar('nama')) {
            $aturan_nama = 'required';
        } else {
            $aturan_nama = 'required|is_unique[penduduk.nama]';
        }
        
        if ($this->validate([
            'nik' => [
                'label' => 'NIK/No.KTP',
                'rules' => $aturan_nik,
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!',
                    'is_unique' => 'Data {field} sudah ada, coba input dengan data berbeda!'
                ]
            ],
            'nama' => [
                'label' => 'Nama Lengkap',
                'rules' => $aturan_nama,
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!',
                    'is_unique' => 'Data {field} sudah ada, coba input dengan data berbeda!'
                ]
            ],
            'foto' => [
                'label' => 'Foto',
                'rules' => 'is_image[foto]|max_size[foto,5000]|mime_in[foto,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'is_image' => 'Berkas {field} harus berupa file gambar atau foto!',
                    'max_size' => 'Ukuran maksimal berkas {field} tidak lebih dari 5MB!',
                    'mime_in' => 'Format berkas {field} hanya mendukung JPG, JPEG and PNG!'
                ]
            ]
        ])) {
            $foto = $this->request->getFile('foto');
            $penduduk = $this->Penduduk->data_id($id);
            if ($foto->getError() == 4) {
                $nama_foto = $penduduk['foto'];
            } else {
                $nama_foto = $foto->getRandomName();
                $foto->move('foto', $nama_foto);
                unlink('foto/' . $penduduk['foto']);
            }
            $data = [
                'id' => $id,
                'nik' => $this->request->getVar('nik'),
                'nama' => $this->request->getVar('nama'),
                'alamat' => $this->request->getVar('alamat'),
                'latitude' => $this->request->getVar('latitude'),
                'longitude' => $this->request->getVar('longitude'),
                'foto' => $nama_foto,
                'id_rw' => $this->request->getVar('id_rw'),
                'id_rt' => $this->request->getVar('id_rt'),
                'jenis_kelamin' => $this->request->getVar('jenis_kelamin')
            ];
            $this->Penduduk->perbarui_data($data);
            session()->setFlashdata('edit_data_success', 'Data berhasil diubah!');
            return redirect()->to('Home/tampil_data');
        } else {
            return redirect()->to('Home/ubah_data/' . $id)->withInput();
        }
    }

    public function hapus_data($id)
    {
        $penduduk = $this->Penduduk->data_id($id);
        if ($penduduk['foto'] <> '') {
            unlink('foto/' . $penduduk['foto']);
        }
        $data = ['id' => $id];
        $this->Penduduk->hapus_data($data);
        session()->setFlashdata('delete_data_success', 'Data telah dihapus!');
        return redirect()->to('Home/tampil_data');
    }

    public function hapus_banyak_data()
    {
        $id_penduduk = $this->request->getVar('id_penduduk');
        for ($hitung = 0; $hitung < count((array)$id_penduduk); $hitung++) {
            $this->Penduduk->delete($id_penduduk[$hitung]);
        }
        return redirect()->to('Home/tampil_data');
    }

    public function rukun_tetangga()
    {
        $id_rw = $this->request->getPost('id_rw');
        $rt = $this->Wilayah->data_rt($id_rw);
        echo '<option value="">--Pilih RT--</option>';
        foreach ($rt as $key => $r) {
            echo "<option value=" . $r['id_rt'] . ">" . $r['kode_rt'] . "</option>";
        }
    }
}
