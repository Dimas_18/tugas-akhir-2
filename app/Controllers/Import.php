<?php

namespace App\Controllers;

use App\Models\Penduduk;
use App\Models\Batas;

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Import extends BaseController
{
    protected $Penduduk;
    protected $Batas;

    public function __construct()
    {
        helper('form');
        $this->Penduduk = new Penduduk();
        $this->Batas = new Batas();
    }

    public function index()
    {
        $data = [
            'judul' => 'Import Data',
            'penduduk' => $this->Penduduk->tampil_data(),
            'menu' => 'kelola_data'
        ];
        return view('KelolaData/ImportData', $data);
    }

    public function import_data()
    {
        $file = $this->request->getFile('file_excel');
        $ext = $file->getClientExtension();

        if ($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        $spreadsheet = $render->load($file);
        $sheet = $spreadsheet->getActiveSheet()->toArray();

        foreach ($sheet as $x => $excel) {
            $nik_penduduk = $this->Penduduk->import_data($excel['0']);
            $nama_penduduk = $this->Penduduk->import_data($excel['1']);
            if ($x == 0) {
                continue;
            } elseif ($excel['0'] == $nik_penduduk['nik']) {
                continue;
            } elseif ($excel['1'] == $nama_penduduk['nama']) {
                continue;
            } elseif ($excel['0'] == null) {
                $data = [
                    'nama' => $excel['1'],
                    'alamat' => $excel['2'],
                    'jenis_kelamin' => $excel['3'],
                    'id_rw' => $excel['4'],
                    'id_rt' => $excel['5'],
                    'latitude' => $excel['6'],
                    'longitude' => $excel['7'],
                ];
    
                $this->Penduduk->masukan_data($data);
            } elseif ($excel['1'] == null) {
                $data = [
                    'nik' => $excel['0'],
                    'alamat' => $excel['2'],
                    'jenis_kelamin' => $excel['3'],
                    'id_rw' => $excel['4'],
                    'id_rt' => $excel['5'],
                    'latitude' => $excel['6'],
                    'longitude' => $excel['7'],
                ];
    
                $this->Penduduk->masukan_data($data);
            }

            $data = [
                'nik' => $excel['0'],
                'nama' => $excel['1'],
                'alamat' => $excel['2'],
                'jenis_kelamin' => $excel['3'],
                'id_rw' => $excel['4'],
                'id_rt' => $excel['5'],
                'latitude' => $excel['6'],
                'longitude' => $excel['7'],
            ];

            $this->Penduduk->masukan_data($data);
        }
        session()->setFlashdata('import_data_success', 'Data berhasil di import!');
        return redirect()->to(base_url('Home/tampil_data'));
    }
}
