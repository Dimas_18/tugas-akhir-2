<?php

namespace App\Controllers;

use App\Models\Penduduk;
use App\Models\Batas;
use App\Models\Jalan;

use App\Controllers\BaseController;

class Map extends BaseController
{
    protected $Penduduk;
    protected $Batas;
    protected $Jalan;
    protected $vb;
    
    public function __construct()
    {
        $this->Penduduk = new Penduduk();
        $this->Batas = new Batas();
        $this->Jalan = new Jalan();
    }
    
    public function index()
    {
        $data = [
            'judul' => 'Peta',
            'vb' => $this->vb,
            'batas' => $this->Batas->findAll(),
            'jalanan' => $this->Jalan->findAll(),
            'penduduk' => $this->Penduduk->tampil_data(),
            'getsegment1' => $this->request->uri->getSegment(1),
            'menu' => 'peta'
        ];
        return view('Peta/index', $data);
    }

    public function cari()
    {
        $kata_kunci = $this->request->getVar('kata_kunci');
        $data = [
            'judul' => 'Cari dalam peta',
            'kata_kunci' => $kata_kunci,
            'vb' => $this->vb,
            'batas' => $this->Batas->cari_data($kata_kunci)->findAll(),
            'jalanan' => $this->Jalan->cari_data($kata_kunci)->findAll(),
            'penduduk' => $this->Penduduk->cari_data($kata_kunci)->findAll(),
            'menu' => 'peta'
        ];
        return view('Peta/index', $data);
    }
}
