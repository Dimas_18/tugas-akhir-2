<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\Situs;

class Other extends BaseController
{
    protected $Situs;

    public function __construct()
    {
        $this->Situs = new Situs();
    }

    public function index()
    {
        $data = [
            'judul' => 'Kelola Website',
            'menu' => 'kelola_web',
            'web' => $this->Situs->detail_data_web(),
        ];
        return view('KelolaWeb', $data);
    }

    public function atur_web()
    {
        if ($this->validate([
            'judul' => [
                'label' => 'Judul',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
            'subjudul' => [
                'label' => 'Sub Judul',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
            'logo' => [
                'label' => 'Logo',
                'rules' => 'max_size[logo,5000]|mime_in[logo,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'max_size' => 'Ukuran maksimal berkas {field} tidak lebih dari 5MB!',
                    'mime_in' => 'Format berkas {field} hanya mendukung JPG, JPEG dan PNG!'
                ]
            ],
            'alamat' => [
                'label' => 'Alamat',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
            'telp' => [
                'label' => 'No. Telp/WA',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
            'email' => [
                'label' => 'Email',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
            'website' => [
                'label' => 'Alamat Website',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
            'tentang' => [
                'label' => 'Tentang',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ]
        ])) {
            $web = $this->Situs->detail_data_web();
            $logo = $this->request->getFile('logo');
            if ($logo->getError() == 4) {
                $nama_logo = $web['logo'];
            } else {
                $nama_logo = $logo->getRandomName();
                $logo->move('gambar', $nama_logo);
                unlink('gambar/'.$web['logo']);
            }
            $data = [
                'id_web' => '1',
                'judul' => $this->request->getPost('judul'),
                'subjudul' => $this->request->getPost('subjudul'),
                'logo' => $nama_logo,
                'alamat' => $this->request->getPost('alamat'),
                'telp' => $this->request->getPost('telp'),
                'email' => $this->request->getPost('email'),
                'website' => $this->request->getPost('website'),
                'tentang' => $this->request->getPost('tentang')
            ];
            $this->Situs->perbarui_data_web($data);
            session()->setFlashdata('update_web_success', 'Data web berhasil diubah!');
            return redirect()->to('Other/index');
        } else {
            session()->setFlashdata('update_web_failed', 'Data web belum berhasil diubah!');
            return redirect()->to('Other/index')->withInput();
        }
    }

    public function tentang()
    {
        $data = [
            'judul' => 'Info Website',
            'menu' => 'tentang',
            'web' => $this->Situs->detail_data_web(),
        ];
        return view('Tentang', $data);
    }
}
