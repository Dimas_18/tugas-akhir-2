<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\Autentikasi;

class User extends BaseController
{
    protected $Autentikasi;

    public function __construct()
    {
        $this->Autentikasi = new Autentikasi();
    }

    public function tampil_user()
    {
        $data = [
            'judul' => 'Data User Autentikasi',
            'user' => $this->Autentikasi->tampil_user(),
            'menu' => 'kelola_pengguna'
        ];
        return view('KelolaPengguna/index', $data);
    }

    public function masukan_user()
    {

        if ($this->validate([
            'nama_asli' => [
                'label' => 'Nama',
                'rules' => 'required|is_unique[users.nama_asli]',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!',
                    'is_unique' => 'Data {field} sudah ada, coba input dengan data berbeda!'
                ]
            ],
            'username' => [
                'label' => 'Username',
                'rules' => 'required|is_unique[users.username]',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!',
                    'is_unique' => 'Data {field} sudah ada, coba input dengan data berbeda!'
                ]
            ],
            'email' => [
                'label' => 'Email',
                'rules' => 'required|is_unique[users.email]',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!',
                    'is_unique' => 'Data {field} sudah ada, coba input dengan data berbeda!'
                ]
            ],
            'password' => [
                'label' => 'Password',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ],
            'level' => [
                'label' => 'Posisi',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Data {field} tidak boleh kosong!'
                ]
            ]
        ])) {
            $data = [
                'nama_asli' => $this->request->getPost('nama_asli'),
                'username' => $this->request->getPost('username'),
                'email' => $this->request->getPost('email'),
                'password' => sha1($this->request->getPost('password')),
                'level' => $this->request->getPost('level')
            ];
            $this->Autentikasi->masukan_user($data);
            session()->setFlashdata('add_user_success', 'Data user berhasil ditambahkan!');
            return redirect()->to('User/tampil_user');
        } else {
            session()->setFlashdata('add_user_failed', 'Data user belum berhasil ditambahkan!');
        }
    }

    public function perbarui_user($id)
    {
        $data = [
            'id' => $id,
            'nama_asli' => $this->request->getPost('nama_asli'),
            'username' => $this->request->getPost('username'),
            'email' => $this->request->getPost('email'),
            'level' => $this->request->getPost('level')
        ];
        $this->Autentikasi->perbarui_user($data);
        session()->setFlashdata('update_user_success', 'Data user berhasil diubah!');
        return redirect()->to('User/tampil_user');
    }

    public function hapus_user($id)
    {
        $data = ['id' => $id];
        $this->Autentikasi->hapus_user($data);
        session()->setFlashdata('delete_user', 'Data user telah dihapus!');
        return redirect()->to('User/tampil_user');
    }

    public function perbarui_password_user($id)
    {
        $data = [
            'id' => $id,
            'password' => sha1($this->request->getPost('password'))
        ];
        $this->Autentikasi->perbarui_user($data);
        session()->setFlashdata('update_password_success', 'Password user berhasil diubah!');
        return redirect()->to('User/tampil_user');
    }
}
