<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\Wilayah;
use App\Models\Penduduk;
use App\Models\Batas;
use App\Models\Jalan;

class Web extends BaseController
{
    protected $Wilayah;
    protected $Penduduk;
    protected $Batas;
    protected $Jalan;
    protected $vb;

    public function __construct()
    {
        $this->Wilayah = new Wilayah();
        $this->Penduduk = new Penduduk();
        $this->Batas = new Batas();
        $this->Jalan = new Jalan();
    }

    public function index()
    {
        $data = [
            'judul' => 'WebGIS Kependudukan',
            'rw' => $this->Wilayah->data_rw(),
            'vb' => $this->vb,
            'batas' => $this->Batas->findAll(),
            'jalanan' => $this->Jalan->findAll(),
            'penduduk' => $this->Penduduk->tampil_data()
        ];
        return view('WebGIS/index', $data);
    }
}
