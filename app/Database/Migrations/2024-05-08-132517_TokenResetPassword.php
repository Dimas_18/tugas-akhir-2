<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TokenResetPassword extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
            ],
            'token' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
            ],
            'waktu_buat timestamp default current_timestamp'
        ]);
        $this->forge->createTable('token_reset');
    }

    public function down()
    {
        $this->forge->dropTable('token_reset');
    }
}
