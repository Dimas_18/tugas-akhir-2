<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if (!function_exists('sendEmail')) {
    function sendEmail($mailConfig)
    {
        require 'PHPMailer/src/Exception.php';
        require 'PHPMailer/src/PHPMailer.php';
        require 'PHPMailer/src/SMTP.php';

        $surat = new PHPMailer(true);
        $surat->SMTPDebug = 0;
        $surat->isSMTP();
        $surat->Host = env('EMAIL_HOST');
        $surat->SMTPAuth = true;
        $surat->Username = env('EMAIL_USERNAME');
        $surat->Password = env('EMAIL_PASSWORD');
        $surat->SMTPSecure = env('EMAIL_ENCRYPTION');
        $surat->Port = env('EMAIL_PORT');
        $surat->setFrom($mailConfig['mail_from_email'], $mailConfig['mail_from_name']);
        $surat->addAddress($mailConfig['mail_recipient_email'], $mailConfig['mail_recipient_name']);
        $surat->isHTML(true);
        $surat->Subject = $mailConfig['mail_subject'];
        $surat->Body = $mailConfig['mail_body'];
        if ($surat->send()) {
            return true;
        } else {
            return false;
        }
    }
}
