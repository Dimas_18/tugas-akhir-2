<?php

namespace App\Models;

use CodeIgniter\Model;

class Autentikasi extends Model
{
    protected $table            = 'users';
    protected $allowedFields    = ['nama_asli', 'username', 'email', 'password'];

    public function login_username_email($username_email)
    {
        return $this->where('username', $username_email)->orWhere('email', $username_email)->first();
    }

    public function login_password($password)
    {
        return $this->db->table('users')->where('password', $password)->get()->getRowArray();
    }

    public function tampil_user()
    {
        return $this->db->table('users')->get()->getResultArray();
    }

    public function masukan_user($data)
    {
        $this->db->table('users')->insert($data);
    }

    public function user_id($id)
    {
        return $this->db->table('users')->where('id', $id)->get()->getRowArray();
    }

    public function perbarui_user($data)
    {
        $this->db->table('users')->where('id', $data['id'])->replace($data);
    }

    public function hapus_user($data)
    {
        $this->db->table('users')->where('id', $data['id'])->delete($data);
    }
}
