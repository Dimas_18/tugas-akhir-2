<?php

namespace App\Models;

use CodeIgniter\Model;

class Batas extends Model
{
    protected $table            = 'batas';
    protected $returnType       = 'object';

    public function cari_data($kata_kunci) {
        return $this->like('nama', $kata_kunci);
    }
}
