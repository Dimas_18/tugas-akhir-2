<?php

namespace App\Models;

use CodeIgniter\Model;

class Jalan extends Model
{
    protected $table            = 'jalan';
    protected $returnType       = 'object';

    public function cari_data($kata_kunci) {
        return $this->like('nama', $kata_kunci)->orLike('jenis', $kata_kunci);
    }
}
