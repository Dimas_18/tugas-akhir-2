<?php

namespace App\Models;

use CodeIgniter\Model;

class Penduduk extends Model
{
    protected $table            = 'penduduk';

    public function masukan_data($data) {
        $this->db->table('penduduk')->insert($data);
    }

    public function tampil_data() {
        return $this->db->table('penduduk')
        ->join('rw', 'rw.id_rw=penduduk.id_rw', 'left')
        ->join('rt', 'rt.id_rt=penduduk.id_rt', 'left')
        ->get()->getResultArray();
    }

    public function data_id($id) {
        return $this->db->table('penduduk')
        ->where('id', $id)
        ->join('rw', 'rw.id_rw=penduduk.id_rw', 'left')
        ->join('rt', 'rt.id_rt=penduduk.id_rt', 'left')
        ->get()->getRowArray();
    }

    public function ambil_data($id = false) {
        if (!$id) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function import_data($nik, $nama) {
        return $this->db->table('penduduk')->where('nik', $nik)->where('nama', $nama)->get()->getRowArray();
    }

    public function perbarui_data($data) {
        $this->db->table('penduduk')->where('id', $data['id'])->replace($data);
    }

    public function hapus_data($data) {
        $this->db->table('penduduk')->where('id', $data['id'])->delete($data);
    }

    public function cari_data($kata_kunci) {
        return $this->like('nik', $kata_kunci)->orLike('nama', $kata_kunci);
    }
}
