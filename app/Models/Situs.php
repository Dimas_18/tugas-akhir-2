<?php

namespace App\Models;

use CodeIgniter\Model;

class Situs extends Model
{
    protected $table            = 'web';

    public function detail_data_web()
    {
        return $this->db->table('web')->where('id_web','1')->get()->getRowArray();
    }

    public function perbarui_data_web($data)
    {
        $this->db->table('web')->where('id_web','1')->update($data);
    }
}
