<?php

namespace App\Models;

use CodeIgniter\Model;

class Wilayah extends Model
{
    public function data_rw()
    {
        return $this->db->table('rw')->get()->getResultArray();
    }

    public function data_rt($id_rw)
    {
        return $this->db->table('rt')->where('id_rw', $id_rw)->get()->getResultArray();
    }

    public function data_rt_2()
    {
        return $this->db->table('rt')->get()->getResultArray();
    }
}
