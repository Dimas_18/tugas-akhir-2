<?php

namespace App\Validation;

class Password
{
    public function password($password): bool
    {
        $password = trim($password);
        if (!preg_match('/^(?=.*[\W])(?=.*[\w])(?=.*[a-zA-Z])(?=.*[0-9]).{3,}$/', $password)) {
            return false;
        }
        return true;
    }
}
