<?= $this->extend('Konten') ?>

<?= $this->section('content') ?>
<div class="page-heading">
  <h3><?= $judul; ?></h3>
</div>

<div class="page-content">
  <div class="col-12">
    <div class="row">
      <div class="col-sm-6">
        <div class="card">
          <div class="card-header">
            <h4>Jumlah Penduduk (gender)</h4>
          </div>
          <div class="card-body">
            <div id="peta" style="width: 100%; height: 50vh;"></div>

          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card">
          <div class="card-body" style="height: 61vh;">
            <canvas id="pieChart"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h4>Jumlah Penduduk (per RW)</h4>
      </div>
      <div class="card-body">
        <canvas id="barChart"></canvas>
      </div>
    </div>
  </div>
</div>

<style>
  .info {
    padding: 6px 8px;
    font: 14px/16px Arial, Helvetica, sans-serif;
    background: white;
    background: rgba(255, 255, 255, 0.8);
    box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
    border-radius: 5px;
  }

  .info h4 {
    margin: 0 0 5px;
    color: #777;
  }

  .legend {
    line-height: 18px;
    color: #555;
  }

  .legend i {
    width: 18px;
    height: 18px;
    float: left;
    margin-right: 8px;
    opacity: 0.7;
  }
</style>

<?php foreach ($rw as $key => $value) {
  $db = \Config\Database::connect();
  $jml_rw[] = $db->table('penduduk')
    ->where('id_rw', $value['id_rw'])
    ->countAllResults();
  $ruwa[] = $value['kode_rw'];
} ?>

<?php foreach ($penduduk as $key => $p) {
  $db = \Config\Database::connect();
  $jk_l = $db->table('penduduk')->where('jenis_kelamin', 'L')->countAllResults();
  $jk_p = $db->table('penduduk')->where('jenis_kelamin', 'P')->countAllResults();
} ?>

<?php include 'KonfigurasiData.php'; ?>

<script>
  var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}{r}.png', {
    maxZoom: 20,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
      '<a href="https://carto.com/attributions">CARTO</a>'
  });

  var peta = L.map('peta', {
    center: [-7.389963691475352, 109.96127545077665],
    zoom: 15,
    layers: [cartodb]
  });

  const info = L.control();

  info.onAdd = function(peta) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
  };

  info.update = function(props) {
    const contents = props ? `<b>${props.Nama}</b><br />${props.Kepadatan} jiwa/km<sup>2</sup>` : 'Hover over a state';
    this._div.innerHTML = `<h4>Kepadatan Penduduk Per RT</h4>${contents}`;
  };

  info.addTo(peta);

  function getColor(d) {
    return d > 0.35 ? '#800026' :
      d > 0.3 ? '#BD0026' :
      d > 0.25 ? '#E31A1C' :
      d > 0.2 ? '#FC4E2A' :
      d > 0.15 ? '#FD8D3C' :
      d > 0.1 ? '#FEB24C' :
      d > 0.05 ? '#FED976' : '#FFEDA0';
  }

  function style(feature) {
    return {
      weight: 2,
      opacity: 1,
      color: 'white',
      dashArray: '3',
      fillOpacity: 0.7,
      fillColor: getColor(feature.properties.Kepadatan)
    };
  }

  function highlightFeature(e) {
    const layer = e.target;

    layer.setStyle({
      weight: 5,
      color: '#666',
      dashArray: '',
      fillOpacity: 0.7
    });

    layer.bringToFront();

    info.update(layer.feature.properties);
  }

  const geojson = L.geoJson(semuaRT, {
    style,
    onEachFeature
  }).addTo(peta);

  function resetHighlight(e) {
    geojson.resetStyle(e.target);
    info.update();
  }

  function zoomToFeature(e) {
    peta.fitBounds(e.target.getBounds());
  }

  function onEachFeature(feature, layer) {
    layer.on({
      mouseover: highlightFeature,
      mouseout: resetHighlight,
      click: zoomToFeature
    });
  }

  const legend = L.control({
    position: 'bottomright'
  });

  legend.onAdd = function(peta) {

    const div = L.DomUtil.create('div', 'info legend');
    const grades = [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35];
    const labels = [];
    let from, to;

    for (let i = 0; i < grades.length; i++) {
      from = grades[i];
      to = grades[i + 1];

      labels.push(`<i style="background:${getColor(from + 1)}"></i> ${from}${to ? `&ndash;${to}` : '+'}`);
    }

    div.innerHTML = labels.join('<br>');
    return div;
  };

  legend.addTo(peta);

  function onMapClick(e) {
    popup.setLatLng(e.latlng).setContent('Koordinat : ${e.latlng.toString()}').openOn(peta);
  }

  peta.on('click', onMapClick);

  const barctx = document.getElementById('barChart');

  new Chart(barctx, {
    type: 'bar',
    data: {
      labels: <?= json_encode($ruwa) ?>,
      datasets: [{
        label: 'Jumlah Penduduk Per RW',
        data: <?= json_encode($jml_rw) ?>,
        backgroundColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(255, 205, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(201, 203, 207, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });

  const piectx = document.getElementById('pieChart');

  new Chart(piectx, {
    type: 'pie',
    data: {
      labels: ['Laki-Laki', 'Perempuan'],
      datasets: [{
        data: [<?= $jk_l = 0 ? 0 : $db->table('penduduk')->where('jenis_kelamin', 'L')->countAllResults(); ?>, <?= $jk_p = 0 ? 0 : $db->table('penduduk')->where('jenis_kelamin', 'P')->countAllResults(); ?>],
        backgroundColor: [
          'rgb(54, 162, 235)',
          'rgb(255, 99, 132)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });
</script>
<?= $this->endSection() ?>