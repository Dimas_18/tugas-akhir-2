<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>A4 landscape</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

    <link rel="stylesheet" href="<?= base_url(''); ?>/paper-css/paper.css">

</head>

<body class="A4 landscape" onload="print()">
    <section class="sheet padding-10mm">
        <center>
            <h3>Laporan Data Penduduk</h3>
        </center>
        <table width="100%" border="">
            <tr>
                <th>No.</th>
                <th>NIK/No. KTP</th>
                <th>Nama Lengkap</th>
                <th>Alamat Lengkap</th>
                <th>RT/RW</th>
                <th>Jenis Kelamin</th>
                <th>Koordinat Lokasi</th>
                <th>Foto KTP/Foto Diri</th>
            </tr>
            <?php $no = 1;
            foreach ($penduduk as $key => $value) { ?>
                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $value['nik'] ?></td>
                    <td><?= $value['nama'] ?></td>
                    <td><?= $value['alamat'] ?></td>
                    <td><?= $value['kode_rt'] ?>/<?= $value['kode_rw'] ?></td>
                    <td><?= $value['jenis_kelamin'] == 'L' ? 'Laki-Laki' : 'Perempuan'; ?></td>
                    <td><?= $value['latitude'] ?>, <?= $value['longitude'] ?></td>
                    <td><img src="<?= base_url('foto/' . $value['foto']); ?>" height="90px"></td>
                </tr>
            <?php } ?>
        </table>
    </section>
</body>

</html>