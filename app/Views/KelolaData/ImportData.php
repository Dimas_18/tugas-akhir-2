<?= $this->extend('Konten') ?>
<?= $this->section('content') ?>

<div class="page-heading">
    <h3><?= $judul ?? "Import Data Penduduk"; ?></h3>
</div>

<div class="container">
    <div class="row">
        <p>Silahkan download dan gunakan template dibawah ini sebagai tempat input data penduduk. (disarankan)</p><br>
        <a class="btn btn-info" href="<?= base_url('template.xlsx'); ?>" role="button">Download Template</a><br>
        <div class="col-12">
            <?php echo form_open_multipart('Import/import_data') ?>
            <div class="form-group">
                <label>Import File</label>
                <input class="form-control" name="file_excel" type="file" accept=".xls,.xlsx">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-sm btn-success">Tambahkan Data</button>
                <a href="<?= base_url('Home/tampil_data'); ?>" class="btn btn-success">Kembali</a>
            </div>
            <?php echo form_close() ?>
        </div>
        <div class="col-12">
            <?php
            if (session()->setFlashdata('pesan')) {
                echo '<div class="alert alert-success" role="alert">';
                echo session()->setFlashdata('pesan');
                echo '</div>';
            }
            ?>
        </div>
    </div>
</div>

<?= $this->endSection() ?>