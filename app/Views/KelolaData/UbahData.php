<?= $this->extend('Konten'); ?>
<?= $this->section('content'); ?>

<div class="row">
    <div class="col-sm-8">
        <div id="peta" style="width: 100%; height: 90vh;"></div>
    </div>

    <div class="col-sm-4">
        <div class="row">
            <?php
            if (session()->getFlashdata('message')) {
                echo '<div class="alert alert-success">';
                echo session()->getFlashdata('message');
                echo '</div>';
            }
            ?>
            <?php $errors = validation_errors() ?>
            <?php echo form_open_multipart('Home/perbarui_data/' . $penduduk['id']) ?>

            <div class="form-group">
                <label>NIK/NO.KTP</label>
                <input class="form-control" name="nik" value="<?= $penduduk['nik'] ?>">
                <p class="text-danger"><?= isset($errors['nik']) == isset($errors['nik']) ? validation_show_error('nik') : '' ?></p>
            </div>

            <div class="form-group">
                <label>Nama Lengkap</label>
                <input class="form-control" name="nama" value="<?= $penduduk['nama'] ?>">
                <p class="text-danger"><?= isset($errors['nama']) == isset($errors['nama']) ? validation_show_error('nama') : '' ?></p>
            </div>

            <div class="form-group">
                <label>Alamat Lengkap</label>
                <input class="form-control" name="alamat" value="<?= $penduduk['alamat'] ?>">
            </div>

            <div class="form-group">
                <label for="">Jenis Kelamin</label>
                <select value="" name="jenis_kelamin" class="form-control">
                    <option value="">--Pilih Jenis Kelamin--</option>
                    <option value="L" <?= $penduduk['jenis_kelamin'] == 'L' ? 'selected' : '' ?>>Laki-Laki</option>
                    <option value="P" <?= $penduduk['jenis_kelamin'] == 'P' ? 'selected' : '' ?>>Perempuan</option>
                </select>
                <p class="text-danger"><?= isset($errors['jenis_kelamin']) == isset($errors['jenis_kelamin']) ? validation_show_error('jenis_kelamin') : '' ?></p>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">RW</label>
                        <select value="<?= old('rw'); ?>" name="id_rw" id="id_rw" class="form-control">
                            <option value="">--Pilih RW--</option>
                            <?php foreach ($rw as $key => $value) { ?>
                                <option value="<?= $value['id_rw'] ?>" <?= $penduduk['id_rw'] == $value['id_rw'] ? 'selected' : '' ?>><?= $value['kode_rw']; ?></option>
                            <?php } ?>
                        </select>
                        <p class="text-danger"><?= isset($errors['id_rw']) == isset($errors['id_rw']) ? validation_show_error('id_rw') : '' ?></p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">RT</label>
                        <select value="<?= old('rt'); ?>" name="id_rt" id="id_rt" class="form-control">
                            <option value="<?= $penduduk['id_rt'] ?>"><?= $penduduk['kode_rt']; ?></option>
                        </select>
                        <p class="text-danger"><?= isset($errors['id_rt']) == isset($errors['id_rt']) ? validation_show_error('id_rt') : '' ?></p>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label>Latitude</label>
                <input class="form-control" name="latitude" id="Latitude" value="<?= $penduduk['latitude'] ?>">
            </div>

            <div class="form-group">
                <label>Longitude</label>
                <input class="form-control" name="longitude" id="Longitude" value="<?= $penduduk['longitude'] ?>">
            </div>

            <div class="form-group">
                <label>Foto</label>
                <input type="file" class="form-control" name="foto" accept="image/*">
                <p class="text-danger"><?= isset($errors['foto']) == isset($errors['foto']) ? validation_show_error('foto') : '' ?></p>
                <img src="<?= base_url('foto/' . $penduduk['foto']); ?>" width="200px">
            </div>

            <br>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="<?= base_url('Home/tampil_data'); ?>" class="btn btn-success">Batal</a>

            <?php echo form_close() ?>
        </div>
    </div>
</div>

<script>
    var defaultmap = L.tileLayer('https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
        attribution: '© Google Maps',
        maxZoom: 20,
    });

    var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
        maxZoom: 19,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://carto.com/attributions">CARTO</a>'
    });

    var jalan = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Map data &copy; <a href="https://www.arcgis.com/">ArcGIS</a>'
    });

    var satellite = L.tileLayer('https://{s}.google.com/vt/lyrs=y&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
        attribution: 'Map data &copy; <a href="https://www.google.com/maps">Google Maps</a>'
    });

    var ground = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
        attribution: 'Map data &copy; <a href="https://www.google.com/maps">Google Maps</a>'
    });

    const peta = L.map('peta', {
        center: [-7.388889078548703, 109.96373235416648],
        zoom: 10,
        layers: [defaultmap]
    });

    const baseLayers = {
        'Default': defaultmap,
        'CartoDB': cartodb,
        'Jalan': jalan,
        'Satelit': satellite,
        'Ketinggian': ground
    };

    const layerControl = L.control.layers(baseLayers, null, {
        collapsed: false
    }).addTo(peta);

    var latInput = document.querySelector("[name=latitude]");
    var lngInput = document.querySelector("[name=longitude]");
    var posInput = document.querySelector("[name=posisi]");
    var lokasi_sekarang = [<?= $penduduk['latitude'] ?>, <?= $penduduk['longitude'] ?>];
    peta.attributionControl.setPrefix(false);

    var tanda = new L.marker(lokasi_sekarang, {
        draggable: true,
    });

    tanda.on('dragend', function(e) {
        var posisi = tanda.getLatLng();
        tanda.setLatLng(posisi, {
            lokasi_sekarang,
        }).bindPopup(posisi).update();
        $("#Latitude").val(posisi.lat);
        $("#Longitude").val(posisi.lng);
    });

    peta.on('click', function(e) {
        var lat = e.latlng.lat;
        var lng = e.latlng.lng;
        if (!tanda) {
            tanda = L.marker(e.latlng).addTo(peta);
        } else {
            tanda.setLatLng(e.latlng);
        }
        latInput.value = lat;
        lngInput.value = lng;
        posInput.value = lat + ', ' + lng;
    });

    peta.addLayer(tanda);

    L.control.locate().addTo(peta);

    function onMapClick(e) {
        popup.setLatLng(e.latlng).setContent('Koordinat : ${e.latlng.toString()}').openOn(peta);
    }

    peta.on('click', onMapClick);
</script>

<script>
    $(document).ready(function() {

        $("#id_rw").change(function(e) {
            var id_rw = $("#id_rw").val();
            $.ajax({
                type: "POST",
                url: "<?= base_url('Home/rukun_tetangga'); ?>",
                data: {
                    id_rw: id_rw
                },
                success: function(response) {
                    $("#id_rt").html(response);
                }
            });
        });

    });
</script>

<?= $this->endSection(); ?>