<?= $this->extend('Konten'); ?>
<?= $this->section('content'); ?>

<div class="page-title">
    <div class="row">
        <div class="col-12 col-md-6 order-md-1 order-last">
            <h3><?= $judul ?? "Data Penduduk"; ?></h3>
        </div>
        <div class="col-12 col-md-6 order-md-2 order-first">
            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                <ol class="breadcrumb">
                </ol>
            </nav>
        </div>
    </div>
</div>
<section class="section">
    <div class="card">
        <div class="card-header">
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                <a href="<?= base_url('Home/hapus_banyak_data'); ?>" class="hapus_semua btn btn-danger" onclick="return confirm('Anda yakin ingin menghapus data yang Anda pilih?')"><i class="fas fa-trash"></i> Hapus Data</a>

                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle me-1" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-print"></i> Cetak Data
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="<?= base_url('Export/cetak_pdf'); ?>" role="button">PDF</a>
                        <a class="dropdown-item" href="<?= site_url('Export/cetak_xlsx'); ?>" role="button">XLSX (Standar Excel)</a>
                    </div>
                </div>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle me-1" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-plus"></i> Tambah Data
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="<?= base_url('Import'); ?>"><i class="fa-solid fa-file-import"></i> Import File Data</a>
                        <a class="dropdown-item" href="<?= base_url('Home/tambah_data'); ?>"><i class="fa-solid fa-pencil"></i> Input Manual</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive datatable-minimal">
                <?php
                if (session()->getFlashdata('add_data_success')) {
                    echo '<div class="alert alert-success alert-dismissible show fade">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                    echo session()->getFlashdata('add_data_success');
                    echo '</div>';
                }
                if (session()->getFlashdata('import_data_success')) {
                    echo '<div class="alert alert-success alert-dismissible show fade">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                    echo session()->getFlashdata('import_data_success');
                    echo '</div>';
                }
                if (session()->getFlashdata('edit_data_success')) {
                    echo '<div class="alert alert-success alert-dismissible show fade">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                    echo session()->getFlashdata('edit_data_success');
                    echo '</div>';
                }
                if (session()->getFlashdata('delete_data_success')) {
                    echo '<div class="alert alert-danger alert-dismissible show fade">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                    echo session()->getFlashdata('delete_data_success');
                    echo '</div>';
                }
                ?>
                <div class="row" id="data_penduduk">
                    <table class="table" id="table2">
                        <thead>
                            <tr>
                                <th class="checkbox text-center">
                                    <input type="checkbox" id="pilih_semua" class="form-check-input">
                                    <label for="pilih_semua"></label>
                                </th>
                                <th>No.</th>
                                <th>Nama Lengkap</th>
                                <th>Alamat Lengkap</th>
                                <th>Jenis Kelamin</th>
                                <th>***</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($penduduk as $key => $value) { ?>
                                <tr>
                                    <input type="hidden" id="id_penduduk" name="id" value="<?= $value['id'] ?>">
                                    <td class="checkbox text-center">
                                        <span class="custom-checkbox">
                                            <input type="checkbox" id="pilih_data" class="form-check-input pilih_data" name="pilih_data" value="<?= $value['id'] ?>">
                                            <label for="pilih_data"></label>
                                        </span>
                                    </td>
                                    <td><?= $no++ ?></td>
                                    <td><?= $value['nama'] ?></td>
                                    <td><?= $value['alamat'] ?>, RT <?= $value['kode_rt'] ?>/ RW <?= $value['kode_rw'] ?></td>
                                    <td><?= $value['jenis_kelamin'] == 'L' ? 'Laki-Laki' : 'Perempuan'; ?></td>
                                    <td>
                                        <a href="<?= base_url($value['id']) ?>" type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#detail-data<?= $value['id'] ?>"><i class="fa-solid fa-magnifying-glass"></i></a>
                                        <a href="<?= base_url('Home/ubah_data/' . $value['id']); ?>" class="btn btn-warning"><i class="fa-solid fa-pen-to-square"></i></a>
                                        <a href="<?= base_url('Home/hapus_data/' . $value['id']); ?>" class="btn btn-danger" onclick="return confirm('Anda yakin ingin menghapus data ini?')"><i class="fas fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php foreach ($penduduk as $key => $value) { ?>
                        <div class="modal fade text-left" id="detail-data<?= $value['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h4 class="modal-title white" id="myModalLabel17">Detail Data Penduduk</h4>
                                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <img src="<?= base_url('foto/' . $value['foto']); ?>" height="180px">
                                        <hr>
                                        <h4><?= $value['nama'] ?></h4>
                                        <p><b>NIK :</b> <?= $value['nik'] ?></p>
                                        <p><b>Alamat :</b> <?= $value['alamat'] ?>, RT <?= $value['kode_rt'] ?>/ RW <?= $value['kode_rw'] ?></p>
                                        <p><b>Jenis Kelamin :</b> <?= $value['jenis_kelamin'] == 'L' ? 'Laki-Laki' : 'Perempuan'; ?></p>
                                        <p><b>Koordinat :</b> <?= $value['latitude'] ?>, <?= $value['longitude'] ?></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary ms-1" data-bs-dismiss="modal">
                                            <i class="bx bx-check d-block d-sm-none"></i>
                                            <span class="d-none d-sm-block">Ok</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {

        $('#pilih_semua').click(function(e) {
            // e.preventDefault();
            if ($(this).is(':checked')) {
                $('.pilih_data').prop('checked', true);
            } else {
                $('.pilih_data').prop('checked', false);
            }
        });

    });

    $(document).on('click', '.hapus_semua', function() {
        var tandai_data = $(".pilih_data:checked");
        if (tandai_data.length > 0) {
            var id_penduduk = [];
            tandai_data.each(function() {
                id_penduduk.push($(this).val());
            })

            $.ajax({
                url: "<?= base_url('Home/hapus_banyak_data'); ?>",
                method: "POST",
                data: {
                    id_penduduk: id_penduduk
                },
                success: function(result) {
                    console.log(result);
                    tandai_data.each(function() {
                        $(this).parent().parent().parent().hide(1000);
                    })
                }
            })
        } else {
            Swal.fire({
                title: "Perhatian!",
                text: "Mohon pilih data yang akan dihapus!",
                icon: "error"
            });
        }
    })
</script>

<?= $this->endSection(); ?>