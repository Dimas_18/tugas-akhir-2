<p>Kepada <?= $surat_data['info']->nama_asli ?></p><br>
<p>
    Password akun WebGIS Desa Kertek Anda telah berhasil diubah. Berikut kredensial login Anda : <br><br>
    <b>Login : </b> <?= $surat_data['info']->username; ?> atau <?= $surat_data['info']->email; ?><br>
    <b>Password : </b> <?= $surat_data['password']; ?><br><br>
    <b>NB: </b> Harap untuk tidak menunjukkan kredensial login Anda ke siapapun!.
</p>