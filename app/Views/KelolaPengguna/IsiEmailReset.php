<p>Kepada <?= $surat_data['info']->nama_asli ?></p><br>
<p>
    Kami telah menerima permintaan Anda untuk reset password untuk akun WebGIS Desa Kertek dengan <i><?= $surat_data['info']->email; ?></i>.<br>
    Anda dapat reset password dengan klik tombol dibawah : <br><br>
    <a href="<?= $surat_data['link']; ?>" style="" target="_blank">Reset Password</a><br><br>
    <b>NB: </b> Batas link valid dalam waktu 15 menit.
</p>