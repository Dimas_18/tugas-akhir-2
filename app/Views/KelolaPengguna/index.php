<?= $this->extend('Konten'); ?>
<?= $this->section('content'); ?>

<div class="page-heading">
    <h3><?= $judul ?? "Data User Autentikasi"; ?></h3>
</div>

<div class="col-md-12">
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">

        <div class="dropdown">

            <a type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#input-manual">
                <i class="fas fa-plus"></i> Tambah User
            </a>

            <div class="modal fade text-left" id="input-manual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Input User Baru</h4>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                <i data-feather="x"></i>
                            </button>
                        </div>

                        <?php
                        if (session()->getFlashdata('add_user_failed')) {
                            echo '<div class="alert alert-danger">';
                            echo session()->getFlashdata('add_user_failed');
                            echo '</div>';
                        }
                        ?>
                        <?php $errors = validation_errors() ?>
                        <?php echo form_open('User/masukan_user') ?>

                        <div class="modal-body">
                            <div class="form form-horizontal">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="first-name-horizontal">Nama</label>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <input id="firs-name-horizontal" class="form-control" name="nama_asli" placeholder="Masukan nama Anda">
                                            <p class="text-danger"><?= isset($errors['nama_asli']) == isset($errors['nama_asli']) ? validation_show_error('nama_asli') : '' ?></p>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="username-horizontal">Username</label>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <input id="username-horizontal" class="form-control" name="username" placeholder="Masukan username Anda">
                                            <p class="text-danger"><?= isset($errors['username']) == isset($errors['username']) ? validation_show_error('username') : '' ?></p>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="email-horizontal">Email</label>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <input type="email" id="email-horizontal" class="form-control" name="email" placeholder="Masukan Email Anda">
                                            <p class="text-danger"><?= isset($errors['email']) == isset($errors['email']) ? validation_show_error('email') : '' ?></p>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="password-horizontal">Password</label>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <input type="password" id="password-horizontal" class="form-control" name="password" placeholder="Masukan password Anda">
                                            <p class="text-danger"><?= isset($errors['password']) == isset($errors['password']) ? validation_show_error('password') : '' ?></p>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="level-horizontal">Posisi</label>
                                        </div>
                                        <fieldset class="form-group col-md-8">
                                            <select class="form-select" id="basicSelect" name="level">
                                                <option value="">--Pilih Posisi--</option>
                                                <option value="1">Admin</option>
                                                <option value="2">User</option>
                                            </select>
                                            <p class="text-danger"><?= isset($errors['level']) == isset($errors['level']) ? validation_show_error('level') : '' ?></p>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-light-secondary" data-bs-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Reset</span>
                            </button>
                            <button type="submit" class="btn btn-primary ms-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Simpan</span>
                            </button>
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div>
            <?php
            if (session()->getFlashdata('add_user_success')) {
                echo '<div class="alert alert-success alert-dismissible show fade">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo session()->getFlashdata('add_user_success');
                echo '</div>';
            }

            if (session()->getFlashdata('update_user_success')) {
                echo '<div class="alert alert-warning alert-dismissible show fade">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo session()->getFlashdata('update_user_success');
                echo '</div>';
            }

            if (session()->getFlashdata('delete_user')) {
                echo '<div class="alert alert-danger alert-dismissible show fade">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo session()->getFlashdata('delete_user');
                echo '</div>';
            }

            if (session()->getFlashdata('update_password_success')) {
                echo '<div class="alert alert-warning alert-dismissible show fade">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo session()->getFlashdata('update_password_success');
                echo '</div>';
            }
            ?>

            <table class="table table-bordered table-sm" id="datatablesSimple">
                <thead>
                    <tr>
                        <th class="text-center">No.</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Posisi</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($user as $key => $value) { ?>
                        <tr>
                            <td class="text-center"><?= $no++ ?></td>
                            <td><?= $value['nama_asli'] ?></td>
                            <td><?= $value['username'] ?></td>
                            <td><?= $value['email'] ?></td>
                            <td><button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#ganti-password<?= $value['id'] ?>"><i class="fa fa-lock"></i> Ganti Password</button></td>
                            <td><?= $value['level'] == 1 ? 'Admin' : 'User'; ?></td>
                            <td class="text-center">
                                <a href="<?= base_url($value['id']) ?>" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#ubah-pengguna<?= $value['id'] ?>">
                                    <i class="fa-solid fa-pen-to-square"></i>
                                </a>
                                <a href="<?= base_url('User/hapus_user/' . $value['id']); ?>" class="btn btn-danger" onclick="return confirm('Anda yakin ingin menghapus data ini?')"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php foreach ($user as $key => $value) { ?>
        <div class="modal fade text-left" id="ubah-pengguna<?= $value['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Ubah Data Pengguna</h4>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>

                    <?php
                    if (session()->getFlashdata('update_user_failed')) {
                        echo '<div class="alert alert-danger">';
                        echo session()->getFlashdata('update_user_failed');
                        echo '</div>';
                    }
                    ?>

                    <?php $errors = validation_errors() ?>
                    <?php echo form_open('User/perbarui_user/' . $value['id']) ?>

                    <div class="modal-body">
                        <div class="form form-horizontal">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="first-name-horizontal">Nama</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input id="firs-name-horizontal" class="form-control" name="nama_asli" placeholder="Masukan nama Anda">
                                        <p class="text-danger"><?= isset($errors['nama_asli']) == isset($errors['nama_asli']) ? validation_show_error('nama_asli') : '' ?></p>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="first-name-horizontal">Username</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input id="first-name-horizontal" class="form-control" name="username" value="<?= $value['username'] ?>" placeholder="Masukan username baru">
                                        <p class="text-danger"><?= isset($errors['username']) == isset($errors['username']) ? validation_show_error('username') : '' ?></p>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="email-horizontal">Email</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="email" id="email-horizontal" class="form-control" name="email" value="<?= $value['email'] ?>" placeholder="Masukan Email baru">
                                        <p class="text-danger"><?= isset($errors['email']) == isset($errors['email']) ? validation_show_error('email') : '' ?></p>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="level-horizontal">Posisi</label>
                                    </div>
                                    <fieldset class="form-group col-md-8">
                                        <select class="form-select form-control" id="basicSelect" name="level">
                                            <option value="">--Pilih Posisi--</option>
                                            <option value="1" <?= $value['level'] == 1 ? 'selected' : '' ?>>Admin</option>
                                            <option value="2" <?= $value['level'] == 2 ? 'selected' : '' ?>>User</option>
                                        </select>
                                        <p class="text-danger"><?= isset($errors['level']) == isset($errors['level']) ? validation_show_error('level') : '' ?></p>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Kembali</span>
                        </button>
                        <button type="submit" class="btn btn-primary ms-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Simpan</span>
                        </button>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php foreach ($user as $key => $value) { ?>
        <div class="modal fade text-left" id="ganti-password<?= $value['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Ubah Password Pengguna</h4>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>

                    <?php
                    if (session()->getFlashdata('update_password_failed')) {
                        echo '<div class="alert alert-danger">';
                        echo session()->getFlashdata('update_password_failed');
                        echo '</div>';
                    }
                    ?>

                    <?php $errors = validation_errors() ?>
                    <?php echo form_open('User/perbarui_password_user/' . $value['id']) ?>

                    <div class="modal-body">
                        <div class="form form-horizontal">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="first-name-horizontal">Username</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input id="first-name-horizontal" class="form-control" name="username" value="<?= $value['username'] ?>" placeholder="Masukan username baru" readonly>
                                        <p class="text-danger"><?= isset($errors['username']) == isset($errors['username']) ? validation_show_error('username') : '' ?></p>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="first-name-horizontal">Password Baru</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input id="first-name-horizontal" type="password" class="form-control" name="password" placeholder="Masukan password baru" require>
                                        <p class="text-danger"><?= isset($errors['password']) == isset($errors['password']) ? validation_show_error('password') : '' ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Kembali</span>
                        </button>
                        <button type="submit" class="btn btn-primary ms-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">Simpan</span>
                        </button>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<?= $this->endSection(); ?>