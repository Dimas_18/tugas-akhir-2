<?= $this->extend('Konten'); ?>
<?= $this->section('content'); ?>

<div class="page-title">
    <div class="row">
        <div class="col-12 col-md-6 order-md-1 order-last">
            <!-- <h3><?= $judul; ?></h3> -->
        </div>
    </div>
</div>

<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                </div>
                <?php
                if (session()->getFlashdata('update_web_success')) {
                    echo '<div class="alert alert-success alert-dismissible show fade">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                    echo session()->getFlashdata('update_web_success');
                    echo '</div>';
                }

                if (session()->getFlashdata('update_web_failed')) {
                    echo '<div class="alert alert-danger alert-dismissible show fade">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                    echo session()->getFlashdata('update_web_failed');
                    echo '</div>';
                }
                ?>
                <?php echo form_open_multipart('Other/atur_web') ?>
                <div class="card-content">
                    <div class="card-body">
                        <form class="form">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Judul</label>
                                        <input type="text" value="<?= $web['judul'] ?>" class="form-control" placeholder="Isikan judul disini." name="judul">
                                        <p class="text-danger"><?= validation_show_error('judul') ?></p>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Sub Judul</label>
                                        <input type="text" value="<?= $web['subjudul'] ?>" class="form-control" placeholder="Isikan subjudul disini." name="subjudul">
                                        <p class="text-danger"><?= validation_show_error('subjudul') ?></p>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Gambar Logo</label>
                                        <input class="form-control" type="file" name="logo" accept="image/*" id="preview_gambar">
                                        <p class="text-danger"><?= validation_show_error('logo') ?></p>
                                        <img src="<?= base_url('gambar/' . $web['logo']); ?>" width="200px" id="proses_gambar" style="border-style: solid; border-width: 1; border-color: yellow;">
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <input value="<?= $web['alamat'] ?>" class="form-control" placeholder="Isikan alamat disini." name="alamat">
                                        <p class="text-danger"><?= validation_show_error('alamat') ?></p>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>No. Telp/WA</label>
                                        <input value="<?= $web['telp'] ?>" class="form-control" placeholder="Isikan no.telp disini." name="telp">
                                        <p class="text-danger"><?= validation_show_error('telp') ?></p>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input value="<?= $web['email'] ?>" class="form-control" placeholder="Isikan email disini." name="email">
                                        <p class="text-danger"><?= validation_show_error('email') ?></p>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Website</label>
                                        <input value="<?= $web['website'] ?>" class="form-control" placeholder="Isikan alamat website disini." name="website">
                                        <p class="text-danger"><?= validation_show_error('website') ?></p>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label>Info Website</label>
                                    <textarea id="summernote" name="tentang"><?= $web['tentang'] ?></textarea>
                                    <p class="text-danger"><?= validation_show_error('tentang') ?></p>
                                </div>
                                <div class="col-12 d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary me-1 mb-1">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</section>

<script>
    function tampil_gambar(input) {
        if (input.files && input.files[0]) {
            var penampil = new FileReader();
            penampil.onload = function(e) {
                $('#proses_gambar').attr('src', e.target.result);
            }
            penampil.readAsDataURL(input.files[0]);
        }
    }

    $('#preview_gambar').change(function() {
        tampil_gambar(this);
    });
</script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            placeholder: 'Ceritakan tujuan, fungsi dan manfaat dari website ini.',
            height: 500
        });
    });
</script>

<?= $this->endSection(); ?>