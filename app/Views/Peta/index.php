<?= $this->extend('Konten'); ?>
<?= $this->section('content'); ?>

<section class="section">
    <div class="row">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">
                    <form method="GET" action="/Map/cari">
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="Cari batas RT, jalan, penduduk..." aria-label="Search" aria-describedby="btnNavbarSearch" name="kata_kunci">
                            <button class="btn btn-primary" id="btnNavbarSearch" type="submit"><i class="fas fa-search"></i></button>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <div id="peta" style="height: 70vh;"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header"><b>Legenda</b></div>
                <div class="card-body">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <div class="form-check">
                                    <div class="checkbox">
                                        <input type="checkbox" id="pilih_jalan" class="form-check-input" checked>
                                    </div>
                                </div>
                            </td>
                            <td><label for="pilih_jalan">Jalan</label></td>
                            <td><img src="<?= base_url('gambar/legenda-peta/jalan.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-check">
                                    <div class="checkbox">
                                        <input type="checkbox" id="pilih_batas_rt" class="form-check-input" checked>
                                    </div>
                                </div>
                            </td>
                            <td><label for="pilih_batas_rt">Batas RT</label></td>
                            <td><img src="<?= base_url('gambar/legenda-peta/batas-rt.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-check">
                                    <div class="checkbox">
                                        <input type="checkbox" id="pilih_penduduk" class="form-check-input" checked>
                                    </div>
                                </div>
                            </td>
                            <td><label for="pilih_penduduk">Penduduk</label></td>
                            <td><img src="<?= base_url('ikon/user location.png'); ?>" height="50"></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-check">
                                    <div class="checkbox">
                                        <input type="checkbox" id="pilih_batas_desa" class="form-check-input" checked>
                                    </div>
                                </div>
                            </td>
                            <td><label for="pilih_batas_desa">Batas Kelurahan/Desa</label></td>
                            <td><img src="<?= base_url('gambar/legenda-peta/batas-desa.png'); ?>" alt=""></td>
                        </tr>
                    </table>
                    <br>
                    <table>
                        <tr>
                            <td>
                                <div class="form-check">
                                    <div class="checkbox">
                                        <input type="checkbox" id="pilih_kepadatan" class="form-check-input" checked>
                                    </div>
                                </div>
                            </td>
                            <th colspan="2">Tingkat Kepadatan Penduduk (Jiwa/Km2)</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                < 10</td>
                            <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/1.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>10 - 15</td>
                            <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/2.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>15 - 20</td>
                            <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/3.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>20 - 25</td>
                            <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/4.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>25 - 30</td>
                            <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/5.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>30 - 35</td>
                            <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/6.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>35 - 40</td>
                            <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/7.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>> 40</td>
                            <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/8.png'); ?>" alt=""></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .info {
        padding: 6px 8px;
        font: 14px/16px Arial, Helvetica, sans-serif;
        background: white;
        background: rgba(255, 255, 255, 0.8);
        box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
        border-radius: 5px;
    }

    .info h4 {
        margin: 0 0 5px;
        color: #777;
    }

    .legend {
        line-height: 18px;
        color: #555;
    }

    .legend i {
        width: 18px;
        height: 18px;
        float: left;
        margin-right: 8px;
        opacity: 0.7;
    }

    .no-background {
        background: transparent;
        border: 0;
        box-shadow: none;
        /* color: #fff; */
    }
</style>

<?php include 'KonfigurasiData.php'; ?>

<script>
    var defaultmap = L.tileLayer('https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
        attribution: '© Google Maps',
        maxZoom: 20,
    });

    var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
        maxZoom: 20,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://carto.com/attributions">CARTO</a>'
    });

    var street = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Map data &copy; <a href="https://www.arcgis.com/">ArcGIS</a>'
    });

    var satellite = L.tileLayer('https://{s}.google.com/vt/lyrs=y&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
        attribution: 'Map data &copy; <a href="https://www.google.com/maps">Google Maps</a>'
    });

    var ground = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
        attribution: 'Map data &copy; <a href="https://www.google.com/maps">Google Maps</a>'
    });

    const baseLayers = {
        'Default': defaultmap,
        'CartoDB': cartodb,
        'Jalan': street,
        'Satelit': satellite,
        'Ketinggian': ground
    };

    const peta = L.map('peta', {
        center: [-7.389963691475352, 109.96127545077665],
        zoom: 15.4,
        layers: [defaultmap]
    });

    const layerControl = L.control.layers(baseLayers, null, {
        collapsed: true
    }).addTo(peta);

    const info = L.control();

    info.onAdd = function(peta) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };

    function getColor(d) {
        return d > 40 ? '#800026' :
            d > 35 ? '#BD0026' :
            d > 30 ? '#E31A1C' :
            d > 25 ? '#FC4E2A' :
            d > 20 ? '#FD8D3C' :
            d > 15 ? '#FEB24C' :
            d > 10 ? '#FED976' : '#FFEDA0';
    }

    $(document).ready(function() {

        function style(feature) {
            return {
                fillOpacity: 0.7,
                fillColor: getColor(feature.properties.Kepadatan)
            };
        }

        function highlightFeature(e) {
            const layer = e.target;

            layer.setStyle({
                weight: 5,
                color: '#666',
                dashArray: '',
                fillOpacity: 0.7
            });

            info.update(layer.feature.properties);
        }

        var wilayahRT = L.geoJSON(semuaRT, {
            style,
            onEachFeature: fiturWilayahRT
        }).addTo(peta);

        $('#pilih_kepadatan').click(function(e) {

            if ($(this).is(':checked')) {
                wilayahRT.addTo(peta);
            } else {
                wilayahRT.remove();
            }
        })

        function resetHighlight(e) {
            wilayahRT.resetStyle(e.target);
            info.update();
        }

        function fiturWilayahRT(feature, layer) {
            layer.bindTooltip(feature.properties.Nama, {
                permanent: true,
                direction: 'center',
                className: 'no-background'
            });

            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                click: zoomToFeature
            });

            layer.bindPopup('<h5>' + feature.properties.Nama + '</h5><hr><b>Luas: </b>' + feature.properties.Luas + ' Km2<br><b>Jumlah Penduduk: </b>' + feature.properties.Penduduk + ' Jiwa<br><b>Kepadatan: </b>' + feature.properties.Kepadatan + ' Jiwa/Km2');
        }
    })

    function zoomToFeature(e) {
        peta.fitBounds(e.target.getBounds());
    }

    var rt = {
        'weight': 3,
        'color': 'black',
        'opacity': 1,
        'dashArray': '7'
    }

    var vb = {
        'color': 'red',
        'opacity': 1,
        'dashArray': '7',
        'weight': 4
    }

    $(document).ready(function() {
        var dataBatasDesa = L.geoJSON({
            "type": "FeatureCollection",
            "features": [<?= $vb; ?>]
        }, {
            style: vb
        }).addTo(peta);

        $('#pilih_batas_desa').click(function(e) {

            if ($(this).is(':checked')) {
                dataBatasDesa.addTo(peta);
            } else {
                dataBatasDesa.remove();
            }
        });
    });

    const people = L.icon({
        iconUrl: '<?= base_url('ikon/user location.png'); ?>',
        iconSize: [50, 60]
    });

    <?php foreach ($penduduk as $key => $value) { ?>

        $(document).ready(function() {
            var dataPenduduk = L.marker([<?= $value['latitude'] ?>, <?= $value['longitude'] ?>], {
                    icon: people
                }).addTo(peta)
                .bindPopup("<img src='<?= base_url('foto/' . $value['foto']); ?>' width='50%'>" + "<h4><?= $value['nama'] ?></h4>" + "<br>Alamat : <?= $value['alamat'] ?>");

            $('#pilih_penduduk').click(function(e) {

                if ($(this).is(':checked')) {
                    dataPenduduk.addTo(peta);
                } else {
                    dataPenduduk.remove();
                }
            });

        });

    <?php } ?>

    <?php foreach ($batas as $key => $rt) { ?>

        $(document).ready(function() {
            var dataBatasRT = L.geoJSON({
                "type": "FeatureCollection",
                "features": [<?= $rt->koordinat_garis; ?>]
            }, {
                style: rt
            }).addTo(peta);

            $('#pilih_batas_rt').click(function(e) {

                if ($(this).is(':checked')) {
                    dataBatasRT.addTo(peta);
                } else {
                    dataBatasRT.remove();
                }
            });

        });
    <?php } ?>

    <?php foreach ($jalanan as $jalanan => $jalan) { ?>

        $(document).ready(function() {
            var dataJalan = L.geoJSON({
                "type": "FeatureCollection",
                "features": [<?= $jalan->koordinat; ?>]
            }, {
                color: 'gray'
            }).addTo(peta);

            $('#pilih_jalan').click(function(e) {

                if ($(this).is(':checked')) {
                    dataJalan.addTo(peta);
                } else {
                    dataJalan.remove();
                }
            });

        });

    <?php } ?>

    L.control.locate().addTo(peta);

    function onMapClick(e) {
        popup.setLatLng(e.latlng).setContent('Koordinat : ${e.latlng.toString()}').openOn(peta);
    }

    peta.on('click', onMapClick);
</script>

<?= $this->endSection(); ?>