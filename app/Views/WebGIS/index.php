<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  $database = \Config\Database::connect();
  $web = $database->table('web')->where('id_web', 1)->get()->getRowArray();
  ?>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>WebGIS | Desa Kertek Wonosobo</title>
  <link rel="shortcut icon" href="<?= base_url('mazer/dist/assets/compiled/svg/favicon.svg'); ?>" type="image/x-icon">
  <link rel="shortcut icon" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAiCAYAAADRcLDBAAAEs2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS41LjAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iCiAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIKICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIgogICAgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIKICAgZXhpZjpQaXhlbFhEaW1lbnNpb249IjMzIgogICBleGlmOlBpeGVsWURpbWVuc2lvbj0iMzQiCiAgIGV4aWY6Q29sb3JTcGFjZT0iMSIKICAgdGlmZjpJbWFnZVdpZHRoPSIzMyIKICAgdGlmZjpJbWFnZUxlbmd0aD0iMzQiCiAgIHRpZmY6UmVzb2x1dGlvblVuaXQ9IjIiCiAgIHRpZmY6WFJlc29sdXRpb249Ijk2LjAiCiAgIHRpZmY6WVJlc29sdXRpb249Ijk2LjAiCiAgIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiCiAgIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSIKICAgeG1wOk1vZGlmeURhdGU9IjIwMjItMDMtMzFUMTA6NTA6MjMrMDI6MDAiCiAgIHhtcDpNZXRhZGF0YURhdGU9IjIwMjItMDMtMzFUMTA6NTA6MjMrMDI6MDAiPgogICA8eG1wTU06SGlzdG9yeT4KICAgIDxyZGY6U2VxPgogICAgIDxyZGY6bGkKICAgICAgc3RFdnQ6YWN0aW9uPSJwcm9kdWNlZCIKICAgICAgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWZmaW5pdHkgRGVzaWduZXIgMS4xMC4xIgogICAgICBzdEV2dDp3aGVuPSIyMDIyLTAzLTMxVDEwOjUwOjIzKzAyOjAwIi8+CiAgICA8L3JkZjpTZXE+CiAgIDwveG1wTU06SGlzdG9yeT4KICA8L3JkZjpEZXNjcmlwdGlvbj4KIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+Cjw/eHBhY2tldCBlbmQ9InIiPz5V57uAAAABgmlDQ1BzUkdCIElFQzYxOTY2LTIuMQAAKJF1kc8rRFEUxz9maORHo1hYKC9hISNGTWwsRn4VFmOUX5uZZ36oeTOv954kW2WrKLHxa8FfwFZZK0WkZClrYoOe87ypmWTO7dzzud97z+nec8ETzaiaWd4NWtYyIiNhZWZ2TvE946WZSjqoj6mmPjE1HKWkfdxR5sSbgFOr9Ll/rXoxYapQVik8oOqGJTwqPL5i6Q5vCzeo6dii8KlwpyEXFL519LjLLw6nXP5y2IhGBsFTJ6ykijhexGra0ITl5bRqmWU1fx/nJTWJ7PSUxBbxJkwijBBGYYwhBgnRQ7/MIQIE6ZIVJfK7f/MnyUmuKrPOKgZLpEhj0SnqslRPSEyKnpCRYdXp/9++msneoFu9JgwVT7b91ga+LfjetO3PQ9v+PgLvI1xkC/m5A+h7F32zoLXug38dzi4LWnwHzjeg8UGPGbFfySvuSSbh9QRqZ6H+Gqrm3Z7l9zm+h+iafNUV7O5Bu5z3L/wAdthn7QIme0YAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAJTSURBVFiF7Zi9axRBGIefEw2IdxFBRQsLWUTBaywSK4ubdSGVIY1Y6HZql8ZKCGIqwX/AYLmCgVQKfiDn7jZeEQMWfsSAHAiKqPiB5mIgELWYOW5vzc3O7niHhT/YZvY37/swM/vOzJbIqVq9uQ04CYwCI8AhYAlYAB4Dc7HnrOSJWcoJcBS4ARzQ2F4BZ2LPmTeNuykHwEWgkQGAet9QfiMZjUSt3hwD7psGTWgs9pwH1hC1enMYeA7sKwDxBqjGnvNdZzKZjqmCAKh+U1kmEwi3IEBbIsugnY5avTkEtIAtFhBrQCX2nLVehqyRqFoCAAwBh3WGLAhbgCRIYYinwLolwLqKUwwi9pxV4KUlxKKKUwxC6ZElRCPLYAJxGfhSEOCz6m8HEXvOB2CyIMSk6m8HoXQTmMkJcA2YNTHm3congOvATo3tE3A29pxbpnFzQSiQPcB55IFmFNgFfEQeahaAGZMpsIJIAZWAHcDX2HN+2cT6r39GxmvC9aPNwH5gO1BOPFuBVWAZue0vA9+A12EgjPadnhCuH1WAE8ivYAQ4ohKaagV4gvxi5oG7YSA2vApsCOH60WngKrA3R9IsvQUuhIGY00K4flQG7gHH/mLytB4C42EgfrQb0mV7us8AAMeBS8mGNMR4nwHamtBB7B4QRNdaS0M8GxDEog7iyoAguvJ0QYSBuAOcAt71Kfl7wA8DcTvZ2KtOlJEr+ByyQtqqhTyHTIeB+ONeqi3brh+VgIN0fohUgWGggizZFTplu12yW8iy/YLOGWMpDMTPXnl+Az9vj2HERYqPAAAAAElFTkSuQmCC" type="image/png">
  <link rel="stylesheet" href="<?= base_url('mazer/dist/assets/css/bootstrap.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('mazer/dist/assets/vendors/perfect-scrollbar/perfect-scrollbar.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('mazer/dist/assets/css/app.css'); ?>">
  <link rel="shortcut icon" href="<?= base_url('mazer/dist/assets/images/favicon.svg'); ?>" type="image/x-icon">
  <link rel="stylesheet" href="<?= base_url('mazer/dist/assets/vendors/simple-datatables/style.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('mazer/dist/assets/compiled/css/app.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('mazer/dist/assets/compiled/css/app-dark.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('mazer/dist/assets/compiled/css/iconly.css'); ?>">
  <script src="<?= base_url('mazer/dist/assets/static/js/components/dark.js'); ?>"></script>
  <script src="<?= base_url('mazer/dist/assets/extensions/perfect-scrollbar/perfect-scrollbar.min.js'); ?>"></script>
  <script src="<?= base_url('mazer/dist/assets/compiled/js/app.js'); ?>"></script>
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css" integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY=" crossorigin="" />
  <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js" integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>
  <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="<?= base_url('mazer/dist/assets/static/js/initTheme.js'); ?>"></script>
  <link rel="stylesheet" href="<?= base_url('locate-control\dist\L.Control.Locate.min.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('search-control\dist\leaflet-search.src.css'); ?>" />
  <script src="<?= base_url('locate-control\dist\L.Control.Locate.min.js'); ?>"></script>
  <script src="<?= base_url('mazer/dist/assets/static/js/pages/horizontal-layout.js'); ?>"></script>
  <script src="<?= base_url('mazer/dist/assets/static/js/pages/dashboard.js'); ?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
  <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>

<body>
  <header>

    <nav class="navbar">
      <a href="<?= base_url('Web'); ?>">
        <table>
          <tr>
            <th rowspan="2" class="text-center"><img src="<?= base_url('gambar/' . $web['logo']); ?>" height="55px"></th>
            <td style="padding-left: 20px; font-size:15px; text-transform:uppercase; text-shadow: 1px 2px;"><?= $web['judul'] ?></td>
          </tr>
          <tr>
            <td style="padding-left: 20px; font-size:15px;"><b><?= $web['subjudul'] ?></b></td>
          </tr>
        </table>
      </a>

      <ul class="nav nav-tabs" id="myTab" role="tablist">

        <li>
          <div class="theme-toggle d-flex gap-2  align-items mt-2">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--system-uicons" width="20" height="20" preserveAspectRatio="xMidYMid meet" viewBox="0 0 21 21">
              <g fill="none" fill-rule="evenodd" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round">
                <path d="M10.5 14.5c2.219 0 4-1.763 4-3.982a4.003 4.003 0 0 0-4-4.018c-2.219 0-4 1.781-4 4c0 2.219 1.781 4 4 4zM4.136 4.136L5.55 5.55m9.9 9.9l1.414 1.414M1.5 10.5h2m14 0h2M4.135 16.863L5.55 15.45m9.899-9.9l1.414-1.415M10.5 19.5v-2m0-14v-2" opacity=".3"></path>
                <g transform="translate(-210 -1)">
                  <path d="M220.5 2.5v2m6.5.5l-1.5 1.5"></path>
                  <circle cx="220.5" cy="11.5" r="4"></circle>
                  <path d="m214 5l1.5 1.5m5 14v-2m6.5-.5l-1.5-1.5M214 18l1.5-1.5m-4-5h2m14 0h2"></path>
                </g>
              </g>
            </svg>
            <div class="form-check form-switch fs-6">
              <input class="form-check-input  me-0" type="checkbox" id="toggle-dark" style="cursor: pointer">
              <label class="form-check-label"></label>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--mdi" width="20" height="20" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
              <path fill="currentColor" d="m17.75 4.09l-2.53 1.94l.91 3.06l-2.63-1.81l-2.63 1.81l.91-3.06l-2.53-1.94L12.44 4l1.06-3l1.06 3l3.19.09m3.5 6.91l-1.64 1.25l.59 1.98l-1.7-1.17l-1.7 1.17l.59-1.98L15.75 11l2.06-.05L18.5 9l.69 1.95l2.06.05m-2.28 4.95c.83-.08 1.72 1.1 1.19 1.85c-.32.45-.66.87-1.08 1.27C15.17 23 8.84 23 4.94 19.07c-3.91-3.9-3.91-10.24 0-14.14c.4-.4.82-.76 1.27-1.08c.75-.53 1.93.36 1.85 1.19c-.27 2.86.69 5.83 2.89 8.02a9.96 9.96 0 0 0 8.02 2.89m-1.64 2.02a12.08 12.08 0 0 1-7.8-3.47c-2.17-2.19-3.33-5-3.49-7.82c-2.81 3.14-2.7 7.96.31 10.98c3.02 3.01 7.84 3.12 10.98.31Z">
              </path>
            </svg>
          </div>
        </li>

        <li class="nav-item" role="presentation">
          <a class="nav-link active" data-bs-toggle="tab" href="#map" role="tab" aria-controls="map" aria-selected="true">Peta</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-bs-toggle="tab" href="#graph" role="tab" aria-controls="graph" aria-selected="false">Grafik</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" data-bs-toggle="tab" href="#about" role="tab" aria-controls="about" aria-selected="false">Tentang Kami</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="btn btn-primary" href="<?= base_url('Auth/login'); ?>" role="button">Login</a>
        </li>
      </ul>
    </nav>

  </header>

  <div class="content-wrapper">
    <div class="page-heading row">
      <?php
      if (session()->getFlashdata('logout')) {
        echo '<div class="alert alert-info alert-dismissible show fade">
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
        echo session()->getFlashdata('logout');
        echo '</div>';
      }
      ?>
      <div class="col-sm-12">
        <div class="tab-content" id="myTabContent">

          <div class="tab-pane fade show active" id="map" role="tabpanel" aria-labelledby="map-tab">
            <div class="row">
              <div class="col-md-9">
                <div id="peta" class="w-100" style="height: 820px;"></div>
              </div>
              <div class="col-md-3">
                <div class="card">
                  <div class="card-content">
                    <div class="card-header">
                      <h4>Legenda</h4>
                    </div>
                    <div class="card-body">
                      <table style="width: 100%;">
                        <tr>
                          <td>
                            <div class="form-check">
                              <div class="checkbox">
                                <input type="checkbox" id="pilih_jalan" class="form-check-input" checked>
                              </div>
                            </div>
                          </td>
                          <td><label for="pilih_jalan">Jalan</label></td>
                          <td><img src="<?= base_url('gambar/legenda-peta/jalan.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                          <td>
                            <div class="form-check">
                              <div class="checkbox">
                                <input type="checkbox" id="pilih_batas_rt" class="form-check-input" checked>
                              </div>
                            </div>
                          </td>
                          <td><label for="pilih_batas_rt">Batas RT</label></td>
                          <td><img src="<?= base_url('gambar/legenda-peta/batas-rt.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                          <td>
                            <div class="form-check">
                              <div class="checkbox">
                                <input type="checkbox" id="pilih_batas_desa" class="form-check-input" checked>
                              </div>
                            </div>
                          </td>
                          <td><label for="pilih_batas_desa">Batas Kelurahan/Desa</label></td>
                          <td><img src="<?= base_url('gambar/legenda-peta/batas-desa.png'); ?>" alt=""></td>
                        </tr>
                      </table>
                      <br>
                      <table style="width: 100%;">
                        <tr>
                          <td>
                            <div class="form-check">
                              <div class="checkbox">
                                <input type="checkbox" id="pilih_kepadatan" class="form-check-input" checked>
                              </div>
                            </div>
                          </td>
                          <th colspan="2">Tingkat Kepadatan Penduduk (Jiwa/Km2)</th>
                        </tr>
                        <tr>
                          <td></td>
                          <td>
                            < 10</td>
                          <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/1.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>10 - 15</td>
                          <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/2.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>15 - 20</td>
                          <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/3.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>20 - 25</td>
                          <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/4.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>25 - 30</td>
                          <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/5.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>30 - 35</td>
                          <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/6.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>35 - 40</td>
                          <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/7.png'); ?>" alt=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>> 40</td>
                          <td><img src="<?= base_url('gambar/legenda-peta/kepadatan/8.png'); ?>" alt=""></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="tab-pane fade" id="graph" role="tabpanel" aria-labelledby="graph-tab">
            <div class="row">
              <div class="col-md-6">
                <div class="card">
                  <div class="card-header">
                    <h4>Jumlah Penduduk (per RW)</h4>
                  </div>
                  <div class="card-body">
                    <canvas id="barChart"></canvas>
                  </div>
                </div>
              </div>
              <div class="col-md-5">
                <div class="card">
                  <div class="card-header">
                    <h4>Jumlah Penduduk (Jenis Kelamin)</h4>
                  </div>
                  <div class="card-body">
                    <div id="pieChart"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="tab-pane fade" id="about" role="tabpanel" aria-labelledby="about-tab">
            <div class="card">
              <div class="card-header">
                <h1>Tentang Kami</h1>
              </div>
              <div class="card-body">
                <!-- <p>Website ini dirancang dengan tujuan untuk memberikan informasi kepada masyarakat terkait kondisi persebaran penduduk beserta tingkat kepadatan penduduk yang ada di Kelurahan/Desa Kertek, Kecamatan Kertek, Kabupaten Wonosobo, Jawa Tengah, Indonesia.</p>
                <p>Adapun informasi yang disajikan di website ini meliputi :</p>
                <ul>
                  <li>Peta kepadatan penduduk beserta informasi geografis lainnya</li>
                  <li>Grafik jumlah penduduk berdasarkan lokasi masing-masing RW</li>
                  <li>Grafik jumlah penduduk berdasarkan jenis kelamin</li>
                </ul> -->
                <?= $web['tentang'] ?>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="page-content">
      <section class="row">
      </section>
    </div>

  </div>

  <footer class="text-center text-bg-primary text-body-primary">

    <div class="card text-bg-primary">
      <div class="card-header text-bg-primary">
        <h2>Informasi Lebih Lanjut</h2>
      </div>
      <div class="container card-body px-4 text-center">
        <div class="row gx-0 justify-content-center">
          <div class="col-sm-4 text-start">
            <div class="p-3">
              <p><i class="fa-solid fa-location-dot"></i> <?= $web['alamat'] ?></p>
              <p><i class="fa-solid fa-square-envelope"></i> <?= $web['email'] ?></p>
              <p><i class="fa-solid fa-square-phone"></i> <?= $web['telp'] ?></p>
              <p><i class="fa-solid fa-globe"></i> <?= $web['website'] ?></p>
            </div>
          </div>
          <div class="col-sm-4 text-end">
            <div class="p-3">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d247.29372264850858!2d109.96653489550687!3d-7.387525035813537!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a9f43c1bd459d%3A0xaa2c0eb526fee221!2sKantor%20Kelurahan%20Kertek!5e0!3m2!1sid!2sid!4v1716265254056!5m2!1sid!2sid" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer text-bg-primary text-body-primary">
        <p>2024 &copy; Kelurahan Kertek Wonosobo 2023</p>
      </div>
    </div>

  </footer>
</body>

<style>
  .info {
    padding: 6px 8px;
    font: 14px/16px Arial, Helvetica, sans-serif;
    background: white;
    background: rgba(255, 255, 255, 0.8);
    box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
    border-radius: 5px;
  }

  .info h4 {
    margin: 0 0 5px;
    color: #777;
  }

  .legend {
    line-height: 18px;
    color: #555;
  }

  .legend i {
    width: 18px;
    height: 18px;
    float: left;
    margin-right: 8px;
    opacity: 0.7;
  }

  .no-background {
    background: transparent;
    border: 0;
    box-shadow: none;
    /* color: #fff; */
  }
</style>

<?php include 'KonfigurasiData.php'; ?>

<?php foreach ($rw as $key => $value) {
  $db = \Config\Database::connect();
  $jml_rw[] = $db->table('penduduk')->where('id_rw', $value['id_rw'])->countAllResults();
  $ruwa[] = $value['kode_rw'];
} ?>

<?php foreach ($penduduk as $key => $p) {
  $db = \Config\Database::connect();
  $jk_l = $db->table('penduduk')->where('jenis_kelamin', 'L')->countAllResults();
  $jk_p = $db->table('penduduk')->where('jenis_kelamin', 'P')->countAllResults();
} ?>

<script>
  var defaultmap = L.tileLayer('https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
    attribution: '© Google Maps',
    maxZoom: 20,
  });

  var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
    maxZoom: 20,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
      '<a href="https://carto.com/attributions">CARTO</a>'
  });

  var street = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
    attribution: 'Map data &copy; <a href="https://www.arcgis.com/">ArcGIS</a>'
  });

  var satellite = L.tileLayer('https://{s}.google.com/vt/lyrs=y&x={x}&y={y}&z={z}', {
    maxZoom: 20,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
    attribution: 'Map data &copy; <a href="https://www.google.com/maps">Google Maps</a>'
  });

  var ground = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', {
    maxZoom: 20,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
    attribution: 'Map data &copy; <a href="https://www.google.com/maps">Google Maps</a>'
  });

  const baseLayers = {
    'Default': defaultmap,
    'CartoDB': cartodb,
    'Jalan': street,
    'Satelit': satellite,
    'Ketinggian': ground
  };

  const peta = L.map('peta', {
    center: [-7.389963691475352, 109.96127545077665],
    zoom: 15.5,
    layers: [defaultmap]
  });

  const layerControl = L.control.layers(baseLayers, null, {
    collapsed: true
  }).addTo(peta);

  const info = L.control();

  info.onAdd = function(peta) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
  };

  function getColor(d) {
    return d > 40 ? '#800026' :
      d > 35 ? '#BD0026' :
      d > 30 ? '#E31A1C' :
      d > 25 ? '#FC4E2A' :
      d > 20 ? '#FD8D3C' :
      d > 15 ? '#FEB24C' :
      d > 10 ? '#FED976' : '#FFEDA0';
  }


  $(document).ready(function() {

    function style(feature) {
      return {
        fillOpacity: 0.7,
        fillColor: getColor(feature.properties.Kepadatan)
      };
    }

    function highlightFeature(e) {
      const layer = e.target;

      layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
      });

      info.update(layer.feature.properties);
    }

    var wilayahRT = L.geoJSON(semuaRT, {
      style,
      onEachFeature: fiturWilayahRT
    }).addTo(peta);

    $('#pilih_kepadatan').click(function(e) {

      if ($(this).is(':checked')) {
        wilayahRT.addTo(peta);
      } else {
        wilayahRT.remove();
      }
    })

    function resetHighlight(e) {
      wilayahRT.resetStyle(e.target);
      info.update();
    }

    function fiturWilayahRT(feature, layer) {
      layer.bindTooltip(feature.properties.Nama, {
        permanent: true,
        direction: 'center',
        className: 'no-background'
      });

      layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
      });

      layer.bindPopup('<h5>' + feature.properties.Nama + '</h5><hr><b>Luas: </b>' + feature.properties.Luas + ' Km2<br><b>Jumlah Penduduk: </b>' + feature.properties.Penduduk + ' Jiwa<br><b>Kepadatan: </b>' + feature.properties.Kepadatan + ' Jiwa/Km2');
    }
  })

  function zoomToFeature(e) {
    peta.fitBounds(e.target.getBounds());
  }

  var rt = {
    'weight': 3,
    'color': 'black',
    'opacity': 1,
    'dashArray': '7'
  }

  var vb = {
    'color': 'red',
    'opacity': 1,
    'dashArray': '7',
    'weight': 4
  }

  $(document).ready(function() {
    var dataBatasDesa = L.geoJSON({
      "type": "FeatureCollection",
      "features": [<?= $vb; ?>]
    }, {
      style: vb
    }).addTo(peta);

    $('#pilih_batas_desa').click(function(e) {

      if ($(this).is(':checked')) {
        dataBatasDesa.addTo(peta);
      } else {
        dataBatasDesa.remove();
      }
    });
  });

  <?php foreach ($batas as $key => $rt) { ?>

    $(document).ready(function() {
      var dataBatasRT = L.geoJSON({
        "type": "FeatureCollection",
        "features": [<?= $rt->koordinat_garis; ?>]
      }, {
        style: rt
      }).addTo(peta);

      $('#pilih_batas_rt').click(function(e) {

        if ($(this).is(':checked')) {
          dataBatasRT.addTo(peta);
        } else {
          dataBatasRT.remove();
        }
      });

    });
  <?php } ?>

  <?php foreach ($jalanan as $jalanan => $jalan) { ?>

    $(document).ready(function() {
      var dataJalan = L.geoJSON({
        "type": "FeatureCollection",
        "features": [<?= $jalan->koordinat; ?>]
      }, {
        color: 'gray'
      }).addTo(peta);

      $('#pilih_jalan').click(function(e) {

        if ($(this).is(':checked')) {
          dataJalan.addTo(peta);
        } else {
          dataJalan.remove();
        }
      });

    });

  <?php } ?>

  L.control.locate().addTo(peta);

  function onMapClick(e) {
    popup.setLatLng(e.latlng).setContent('Koordinat : ${e.latlng.toString()}').openOn(peta);
  }

  peta.on('click', onMapClick);
</script>

<script>
  new Chart("barChart", {
    type: "bar",
    data: {
      labels: <?= json_encode($ruwa) ?>,
      datasets: [{
        label: 'Jumlah Penduduk (Jiwa)',
        backgroundColor: [
          "rgb(54, 162, 235)",
          "green",
          "rgb(255, 99, 132)",
          "rgb(255, 205, 86)",
          "brown"
        ],
        data: <?= json_encode($jml_rw) ?>
      }]
    },
    options: {
      legend: {
        display: false
      },
      title: {
        display: true,
      }
    }
  });
</script>

<script>
  const data = [{
    labels: ['Laki-Laki', 'Perempuan'],
    values: [<?= $jk_l = 0 ? 0 : $db->table('penduduk')->where('jenis_kelamin', 'L')->countAllResults(); ?>, <?= $jk_p = 0 ? 0 : $db->table('penduduk')->where('jenis_kelamin', 'P')->countAllResults(); ?>],
    type: "pie"
  }];

  Plotly.newPlot("pieChart", data, null);
</script>

</html>